<?php
include ('./partials-front/header.php');
    //Kiểm tra xem id food đã được đặt hay chưa
    if (isset($_GET['food_id'])) {
        //Lấy id food và lấy thông tin chi tiết của món đã chọn
        $food_id = $_GET['food_id'];
        //Lấy thông tin chi tiết của món ăn đã chọn
        $sql = "SELECT * FROM tbl_food WHERE id=$food_id";
        //Thực thi truy vân
        $res = mysqli_query($conn, $sql);
        //Lấy số hàng trong một kết quả
        $count = mysqli_num_rows($res);
        //Kiểm tra xem dữ liệu có sẵn hay không
        if ($count == 1) {
            //Lấy dữ liệu từ db
            $row = mysqli_fetch_assoc($res);
            $title = $row['title'];
            $price = $row['price'];
            $image_name = $row['image_name'];
        } else {
            //Món ăn không có 
            //Về trang chủ
            header('location:' . SITEURL);
        }
    } else {
        //Về trang chủ
        header('location:' . SITEURL);
    }
?>
    <div class="order">
        <h2 class="text-center text-white">Điền thông tin để xác nhận đơn đặt hàng.</h2>
        <div class="order-wrapper">

            <form action="" method="POST">
                <fieldset class="food-info">
                    <legend>Chọn món</legend>
                    <div class="order-img">
                    <?php
                    //Kiểm tra xem hình ảnh có sẵn hay không
                    if ($image_name == "") {
                        //Hình ảnh không có sẵn
                        echo '<div class="error">Hình ảnh không có sẵn.</div>';
                    } else {
                    ?>
                        <img src="<?php echo SITEURL; ?>assets/img/food/<?php echo $image_name; ?>" >
                    <?php } ?>
                    </div>
                    <div class="order-desc">
                        <h3><?php echo $title; ?></h3>
                        <input type="hidden" name="food" value="<?php echo $title; ?>">
                        <p class="food-price"><?php echo number_format($price,3 ,'.','.'); ?>đ</p>
                        <input type="hidden" name="price" value="<?php echo $price; ?>">
                        <label>Số lượng: </label>
                        <input type="number" name="qty" min="1" value="1" required>
                    </div>
                </fieldset>
                <fieldset class="food-desc">
                    <legend>Chi tiết giao hàng</legend>
                    <div class="order-info">
                        <label class="order-label" for="full-name">Họ và tên: </label>
                        <input type="text" name="full-name" id="full-name" placeholder="Vd: Nguyễn Văn A" required>
                    </div>
                    <div class="order-info">
                        <label class="order-label" for="contact">Số điện thoại: </label>
                        <input onkeyup="checkPhone();" type="phone" name="contact" id="contact" placeholder="Vd: 0372642836" minlength="10" maxlength="10" required>
                        <div class="icons">
                            <span class="icon1 fas fa-exclamation"></span>
                            <span class="icon2 fas fa-check"></span>
                        </div>
                    </div>
                    <div class="error-text-phone">Vui lòng nhập đúng số điện thoại</div>
                    <div class="order-info">
                        <label class="order-label"  for="email">Email: </label>
                        <input onkeyup="checkMail();" type="email" name="email"  id="email" placeholder="Vd: abcdef@gmail.com" required>
                        <div class="icons">
                            <span class="icon3 fas fa-exclamation"></span>
                            <span class="icon4 fas fa-check"></span>
                        </div>
                    </div>
                    <div class="error-text-mail">Vui lòng nhập đúng địa chỉ email</div>
                    <div class="order-info">
                        <label class="order-label" for="address">Địa chỉ: </label>
                        <textarea name="address" id="address" rows="10" placeholder="Vd: Số 2, đường Số 10" required></textarea>
                    </div>
                    <input type="submit" id="submit-order" name="submit" value="Xác nhận đặt hàng" class="btn-order">
                </fieldset>
            </form>
            
            <?php
            //Kiểm tra xem số lượng có phải là số và có dương hay không
            if (isset($_POST['qty'])) {
                if(is_numeric($_POST['qty']) == 1 && $_POST['qty'] >= 1) {
                    //Kiểm tra xem nút submit click hay chưa 
                    if (isset($_POST['submit'])) {
                        // Lấy all thông tin từ form
                        $food = $_POST['food'];
                        $price = $_POST['price'];
                        $qty = $_POST['qty'];
                        $total = $price * $qty; // tổng = giá * sl
                        $order_date = date("Y-m-d H:i:s A"); //Ngày order
                        $status = "Ordered";  // Ordered, On Delivery, Delivered, Cancelled
                        $customer_name = $_POST['full-name'];
                        $customer_contact = $_POST['contact'];
                        $customer_email = $_POST['email'];
                        $customer_address = $_POST['address'];
                        //Lưu đơn hàng vào db
                        //Tạo sql để lưu dữ liệu
                        $sql2 = "INSERT INTO tbl_order SET 
                            food = '$food',
                            price = $price,
                            qty = $qty,
                            total = $total,
                            order_date = '$order_date',
                            status = '$status',
                            customer_name = '$customer_name',
                            customer_contact = '$customer_contact',
                            customer_email = '$customer_email',
                            customer_address = '$customer_address'
                        ";
                        //echo $sql2; die();
                        //Thực thi truy vấn
                        $res2 = mysqli_query($conn, $sql2);
                        //Kiểm tra xem truy vấn có được thực thi hay không
                        if ($res2 == true) {
                            //Lưư đơn hàng
                            $_SESSION['order'] = '<script>
                            setTimeout(() => {
                                const notify = document.querySelector("#notify");
                                notify.onclick = () => {
                                    showSuccessToast("Đặt món ăn thành công !");
                                }
                                window.onload = notify.click();
                            }, 100);
                            </script>';
                            header('location:' . SITEURL);
                        } else {
                            //Lưu đơn hàng thất bại
                            $_SESSION['order'] = '<script>
                            setTimeout(() => {
                                const notify = document.querySelector("#notify");
                                notify.onclick = () => {
                                    showSuccessToast("Đặt món ăn không thành công !");
                                }
                                notify.click();
                            }, 100);
                            </script>';
                            header('location:' . SITEURL);
                        }
                    }
                }
            }

          
            ?>
        </div>
    </div>
    <?php include ('./partials-front/footer.php'); ?>
    <a href="#" class="back-to-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <script src="./assets/js/main.js"></script>
    <script src="./assets/js/check-input.js"></script>

</body>

</html>