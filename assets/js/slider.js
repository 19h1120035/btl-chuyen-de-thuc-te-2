// slide show start
document.getElementById('radio1').checked = true;
var counter = 2;
setInterval(function() {
    document.getElementById('radio' + counter).checked = true;
    counter++;
    if (counter > 4) {
        counter = 1;
    }
}, 4000);
// slide show end

const prevBtn = document.querySelector(".slide-prev");
const nextBtn = document.querySelector(".slide-next");
var count = 1;
prevBtn.onclick = () => {
    count--;
    if (count < 1) {
        count = 4;
    }
    document.getElementById('radio' + count).checked = true;
};
nextBtn.onclick = () => {
    count++;
    if (count > 4) {
        count = 1;
    }
    document.getElementById('radio' + count).checked = true;
};