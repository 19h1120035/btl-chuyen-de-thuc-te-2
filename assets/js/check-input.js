const email = document.querySelector("#email");
const phone = document.querySelector("#contact");
const icon1 = document.querySelector(".icon1");
const icon2 = document.querySelector(".icon2");
const icon3 = document.querySelector(".icon3");
const icon4 = document.querySelector(".icon4");
const textErrorMail = document.querySelector(".error-text-mail");
const textErrorPhone = document.querySelector(".error-text-phone");
const btnSubmitOrder = document.querySelector("#submit-order");
let regexMail = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/;
let regexPhone = /^(03|08|09)\d{8}$/;
icon1.style.display = "none";
icon2.style.display = "none";
icon3.style.display = "none";
icon4.style.display = "none";
textErrorMail.style.display = "none";
textErrorPhone.style.display = "none";

function checkInput(input, regex, iconError, iconSuccess, textError) {
    if (input.value.match(regex)) {
        input.style.border = "1px solid #27ae60";
        input.style.backgroundColor = "rgb(195,231,193)";
        iconError.style.display = "none";
        iconSuccess.style.display = "block";
        textError.style.display = "none";
    } else {
        input.style.border = "1px solid #e74c3c";
        input.style.backgroundColor = "rgb(225,193,190)";
        iconError.style.display = "block";
        iconSuccess.style.display = "none";
        textError.style.display = "block";
    }
    if (input.value == "") {
        input.style.border = "none";
        input.style.backgroundColor = "#fff";
        iconError.style.display = "none";
        iconSuccess.style.display = "none";
        textError.style.display = "none";
    }
    if (textErrorMail.style.display == "block" || textErrorPhone.style.display == "block") {
        btnSubmitOrder.disabled = true;
        btnSubmitOrder.style.cursor = "default";
    } else {
        btnSubmitOrder.disabled = false;
        btnSubmitOrder.style.cursor = "pointer";
    }
}

function checkPhone() {
    checkInput(phone, regexPhone, icon1, icon2, textErrorPhone);
}

function checkMail() {
    checkInput(email, regexMail, icon3, icon4, textErrorMail);
}