// back to top start
const toTop = document.querySelector(".back-to-top");
window.addEventListener("scroll", () => {
    if (window.pageYOffset > 60) {
        toTop.classList.add("active");
    } else {
        toTop.classList.remove("active");
    }
});

//back to top end
// nav mobile

const navbar = document.querySelector(".menu-toggle");
const navMenu = document.querySelector(".nav-mobile");
navbar.onclick = () => {
    if (navMenu.classList.contains("active")) {
        navMenu.classList.remove("active");
    } else {
        navMenu.classList.add("active");
    }
};
// Header start
window.onscroll = () => {
    var header = document.querySelector("header");
    header.classList.toggle("sticky", window.scrollY > 60)
    if (navMenu.classList.contains("active")) {
        navMenu.classList.remove("active");
    }
};
// Header end
const navLinks = document.querySelectorAll('.nav-link');
const navLinksMobile = document.querySelectorAll(".nav-mobile-link");

function navChangeColor(navs, color) {
    navs.forEach(navLink => navLink.onclick = () => {
        navs.forEach(item => {
            item.style.color = "#333";
        });
        navLink.style.color = color;
    });
}
navChangeColor(navLinks, "#008080");
navChangeColor(navLinksMobile, "red");

// handle click đật hàng
const foodItems = document.querySelectorAll(".food-item");
foodItems.forEach(item => item.onclick = () => {
    item.querySelector(".btn-order").click();
});
// end đặt hàng