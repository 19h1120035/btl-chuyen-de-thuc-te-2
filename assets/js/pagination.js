// start phân trang
// pages.forEach(page => page.onclick = function() {
//     for (let i = 0; i < pages.length; i++) {
//         const item = pages[i];
//         item.classList.remove("page-checked");
//     }
//     page.classList.add("page-checked");
// });
const pages = document.querySelectorAll(".page");
const pagePrev = document.querySelector(".page-prev");
const pageNext = document.querySelector(".page-next");
var currentPage = 1;

function changePage(number) {
    for (let i = 0; i < pages.length; i++) {
        if (pages[i].classList.contains("page-checked")) {
            currentPage = pages[i].textContent;
        }
    }
    if (number == 1) {
        if (currentPage < pages.length) {
            pages[currentPage - 1].classList.remove("page-checked");
            pages[currentPage].classList.add("page-checked");
            pages[currentPage].click();
        }
    } else if (number == -1) {
        if (currentPage > 1) {
            pages[currentPage - 1].classList.remove("page-checked");
            pages[currentPage - 2].classList.add("page-checked");
            pages[currentPage - 2].click();
        }

    }
}

pagePrev.onclick = () => {
    changePage(-1);
};

pageNext.onclick = () => {
    changePage(1);
};
// end phân trang