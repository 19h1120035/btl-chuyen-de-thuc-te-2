<?php
include('./fronts/header.php');
include('./fronts/navbar.php');
?>
<script>
    const navs = document.querySelectorAll('.sub-nav');
    navs[2].style.color = 'red';
</script>
<?php
//Kiểm tra id đã đặt chưa
if (isset($_GET['id'])) {
    //lấy id food
    $id = $_GET['id'];
    //Truy vấn sql để lấy món ăn đã chọn
    $sql2 = "SELECT * FROM tbl_food WHERE id=$id";
    $res2 = mysqli_query($conn, $sql2);
    //Lấy giá trị dựa trên truy vấn được thực thi
    $row2 = mysqli_fetch_assoc($res2);
    //Lấy từng giá trị của món ăn
    $title = $row2['title'];
    $description = $row2['description'];
    $price = $row2['price'];
    $current_image = $row2['image_name'];
    $current_category = $row2['category_id'];
    $featured = $row2['featured'];
    $active = $row2['active'];
} else {
    //Về trang manage
    $_SESSION['update'] = '<script>
    setTimeout(() => {
        const notify = document.querySelector("#notify");
        notify.onclick = () => {
            showErrorToast("Cập nhật món ăn thất bại !");
        }
        notify.click();
    }, 100);
    </script>';
    header('location:' . SITEURL . 'admin/food-manage.php');
}
?>
    <div class="add">
        <div class="form-add">
            <h1 class="title text-center">Cập Nhật Ẩm Thực</h1>
            <form action="" method="POST" enctype="multipart/form-data">
                <div class="form-item">
                    <label class="form-label" for="title">Tên món ăn: </label>
                    <input class="form-input" type="text" name="title" id="title" placeholder="Nhập tên món ăn" required value="<?php echo $title; ?>">
                </div>
                <div class="form-item">
                    <label class="form-label" for="description">Mô tả: </label>
                    <textarea class="form-input" name="description" id="description" cols="30" rows="5" placeholder="Nhập mô tả món ăn" required><?php echo $description; ?></textarea>

                </div>
                <div class="form-item">
                    <label class="form-label" for="price">Giá tiền: </label>
                    <input class="form-input" type="number" name="price" id="price" min="1" placeholder="Nhập giá tiền" value="<?php echo $price; ?>">
                </div>
                <div class="form-item">
                    <label class="form-label" for="">Ảnh hiện tại: </label>
                    <?php
                    if ($current_image == "") {
                        echo "<div class='error'>Hình ảnh không có sẵn. </div>";
                    } else {
                    ?>
                        <img class="form-img" src="<?php echo SITEURL; ?>/assets/img/food/<?php echo $current_image; ?>">
                </div>
                <div class="form-item">
                    <label class="form-label" for="image">Chọn ảnh mới: </label>
                    <input class="form-file" type="file" id="image" name="image">
                </div>
                <div class="form-item">
                    <label class="form-label" for="category">Danh mục: </label>
                    <select class="form-input" name="category" id="category">
                        <?php
                        //Truy vấn sql để lấy danh mục hoạt động
                        $sql = "SELECT * FROM tbl_category WHERE active='Yes'";
                        $res = mysqli_query($conn, $sql);
                        //đếm 
                        $count = mysqli_num_rows($res);
                        //Kiểm tra xem danh mục có trong db không
                        if ($count > 0) {
                            while ($row = mysqli_fetch_assoc($res)) {
                                $category_title = $row['title'];
                                $category_id = $row['id'];
                        ?>
                                <option <?php if ($current_category == $category_id) {
                                            echo "selected";
                                        } ?> value="<?php echo $category_id; ?>"><?php echo $category_title; ?></option>
                        <?php
                            }
                        } else {
                            //Danh mục không có sẵn 
                            echo "<option class='error' value='0'>Danh mục không có sẵn .</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="form-item">
                    <label class="form-label" for="featured">Đặc trưng: </label>
                    <div class="form-check">
                        <input <?php if ($featured == "Yes") {
                                    echo "checked";
                                } ?> type="radio" checked name="featured" id="featured-yes" value="Yes"><label for="featured-yes">Có</label>
                        <input <?php if ($featured == "No") {
                                    echo "checked";
                                } ?> type="radio" name="featured" id="featured-no" value="No"><label for="featured-no">Không</label>
                    </div>
                </div>
                <div class="form-item">
                    <label class="form-label" for="active">Hoạt động: </label>
                    <div class="form-check">
                        <input <?php if ($active == "Yes") {
                                    echo "checked";
                                } ?> type="radio" checked name="active" id="yes" value="Yes"><label for="yes">Có</label>
                        <input <?php if ($active == "No") {
                                    echo "checked";
                                } ?> type="radio" name="active" id="no" value="No"><label for="no">Không</label>
                    </div>
                </div>
                <input type="hidden" name="id" value="<?php echo $id; ?>">
                <input type="hidden" name="current_image" value="<?php echo $current_image; ?>">
                <input type="submit" name="submit" value="Lưu" class="form-btn">
            </form>
        <?php
        if (isset($_POST['submit'])) {
            //1. Lấy all thông tin từ form
            $id = $_POST['id'];
            $title = $_POST['title'];
            $description = $_POST['description'];
            $price = $_POST['price'];
            $current_image = $_POST['current_image'];
            $category = $_POST['category'];
            $featured = $_POST['featured'];
            $active = $_POST['active'];
            //2. Tải ảnh lên nếu được chọn
            //Kiểm tra nút tải ảnh lên có được click hay không
            if (isset($_FILES['image']['name'])) {
                $image_name = $_FILES['image']['name'];
                //Kiểm tra xem tệp có sẵn không
                if ($image_name != "") {
                    //Tải lên ảnh mới
                    //Đổi tên 
                    $ext = end(explode('.', $image_name)); //Lấy phần mở rộng của ảnh. vd: jpg,png...

                    $image_name = "Food-Name-" . rand(0000, 9999) . '.' . $ext; //Đổi tên ảnh

                    //Lấy đường dẫn nguồn và đường dẫn đích 
                    $src_path = $_FILES['image']['tmp_name'];
                    $dest_path = "../assets/img/food/" . $image_name;

                    //Tải ảnh lên
                    $upload = move_uploaded_file($src_path, $dest_path);

                    /// Kiểm tra xem hình ảnh có được tải lên hay không 
                    if ($upload == false) {
                        $_SESSION['upload'] = '<script>
                        setTimeout(() => {
                            const notify = document.querySelector("#notify");
                            notify.onclick = () => {
                                showErrorToast("Không tải lên được hình ảnh mới !");
                            }
                            notify.click();
                        }, 100);
                        </script>';
                        header('location:' . SITEURL . 'admin/food-manage.php');
                        die();
                    }
                    // 3. Xóa hình ảnh nếu hình ảnh mới và hình ảnh hiện tại tồn tại 
                    //B. Xóa hình ảnh hiện tại nếu có
                    if ($current_image != "") {
                        $remove_path = "../assets/img/food/" . $current_image;
                        $remove = unlink($remove_path);
                        //Kiểm tra hình ảnh có được xóa hay không
                        if ($remove == false) {
                            $_SESSION['remove-failed'] = '<script>
                            setTimeout(() => {
                                const notify = document.querySelector("#notify");
                                notify.onclick = () => {
                                    showErrorToast("Không thể xóa ảnh hiện tại !");
                                }
                                notify.click();
                            }, 100);
                            </script>';
                            header('location:' . SITEURL . 'admin/food-manage.php');
                            die();
                        }
                    }
                } else {
                    $image_name = $current_image; //Mặc định tên ảnh khi ảnh kh được chọn
                }
            } else {
                $image_name = $current_image;
            }
            //4. Update món ăn vào db
            $sql3 = "UPDATE tbl_food SET 
            title = '$title',
            description = '$description',
            price = $price,
            image_name = '$image_name',
            category_id = '$category',
            featured = '$featured',
            active = '$active'
            WHERE id=$id
            ";
            $res3 = mysqli_query($conn, $sql3);

            //Kiểm tra truy vấn có được thực thi hay không
            if ($res3 == true) {
                $_SESSION['update'] = '<script>
                setTimeout(() => {
                    const notify = document.querySelector("#notify");
                    notify.onclick = () => {
                        showSuccessToast("Cập nhật món ăn thành công !");
                    }
                    notify.click();
                }, 100);
                </script>';
                header('location:' . SITEURL . 'admin/food-manage.php');
            } else {
                $_SESSION['update'] = '<script>
                setTimeout(() => {
                    const notify = document.querySelector("#notify");
                    notify.onclick = () => {
                        showErrorToast("Cập nhật món ăn thất bại !");
                    }
                    notify.click();
                }, 100);
                </script>';
                header('location:' . SITEURL . 'admin/food-manage.php');
            }
        }
    }
?>
        </div>
    </div>
    </body>

    </html>