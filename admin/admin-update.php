    <?php
    include('./fronts/header.php');
    include('./fronts/navbar.php');
    ?>
    <script>
        const navs = document.querySelectorAll('.sub-nav');
        navs[4].style.color = 'red';
    </script>
    <?php
if ($_SERVER['REQUEST_METHOD'] === "POST") {
    $error = array();
    if (empty($_POST['full_name'])) {
        $error['full_name'] = "Vui lòng nhập họ và tên";
    } else {
        $full_name = $_POST['full_name'];
        if (strlen($_POST['full_name']) < 2) {
            $error['full_name'] = "Họ và tên tối thiểu 2 kí tự";
        } else {
            $full_name_new = $_POST['full_name'];
        }
    }

    if (empty($_POST['username'])) {
        $error['username'] = "Vui lòng nhập tên đăng nhập";
    } else {
        if (strlen($_POST['username']) < 2) {
            $error['username'] = "Tên đăng nhập tối thiểu 2 kí tự";
        } else {
            $username_new = $_POST['username'];
        }
    }
}
?>
    <div class="add">
        <div class="form-add">
            <h1 class="title text-center">Cập Nhật Quản Trị Viên</h1>
            <?php
        //1. Lấy id admin đã chọn 
        $id = $_GET['id'];

        //2.Tạo truy vấn SQL để lấy chi tiết
        $sql = "SELECT * FROM tbl_admin WHERE id=$id";

        //Thực thi truy vấn
        $res = mysqli_query($conn, $sql);

        //Check xem truy vấn có được thực thi hay không
        if ($res == true) {
            // Kiểm tra xem dữ liệu có sẵn hay không 
            $count = mysqli_num_rows($res);
            //Check xem có dữ liệu admin hay không
            if ($count == 1) {
                // Lấy chi tiết
                // echo "Admin có sẵn"; 
                $row = mysqli_fetch_assoc($res);
                $full_name = $row['full_name'];
                $username = $row['username'];
            } else {
                //chuyển hướng về trang quản lý admin
                header('location:' . SITEURL . 'admin/admin-manage.php');
            }
        }
        ?>
            <form action="" method="POST" enctype="multipart/form-data">
                <div class="form-item">
                    <label class="form-label" for="full_name">Họ và tên: </label>
                    <input class="form-input" type="text" id="full_name" name="full_name" placeholder="VD: Nguyễn Văn A" value="<?php if (isset($full_name_new)) { echo $full_name_new;} else { echo $full_name;}?>">
                    <?php
                if (isset($error['full_name'])) {
                ?>
                    <span style="color: red;"><?php echo $error['full_name']; ?></span>
                <?php } ?>
                </div>
                <div class="form-item">
                    <label class="form-label" for="username">Tên đăng nhập: </label>
                    <input class="form-input" type="text" id="username" name="username" placeholder="VD: admin" value="<?php if (isset($username_new)) { echo $username_new; } else { echo $username; }?>">
                    <?php
                if (isset($error['username'])) {
                ?>
                    <span style="color: red;"><?php echo $error['username']; ?></span>
                <?php } ?>
                </div>
                <input type="hidden" name="id" value="<?php echo $id; ?>">
                <input type="submit" name="submit" value="Lưu" class="form-btn">
            </form>
        </div>
    </div>
    <?php
if (isset($full_name_new) && isset($username_new)) {
    if (!empty($full_name_new && $username_new)) {
        //Kiểm tra xem nút Gửi có được nhấp vào hay không
        if (isset($_POST['submit'])) {
            // Lấy tất cả các giá trị từ form để cập nhật 
            $id = $_POST['id'];
            $full_name = $full_name_new;
            $username = $username_new;
            // Tạo truy vấn SQL để cập nhật Admin
            $sql = "UPDATE tbl_admin SET full_name = '$full_name', username = '$username' WHERE id='$id' ";
            // Thực thi truy vấn
            $res = mysqli_query($conn, $sql);
            //Kiểm tra xem truy vấn có được thực thi thành công hay không 
            if ($res == true) {
                //Truy vấn được thực thi và cập nhật admin 
                $_SESSION['update'] = '<script>
                setTimeout(() => {
                    const notify = document.querySelector("#notify");
                    notify.onclick = () => {
                        showSuccessToast("Cập nhật admin thành công !");
                    }
                    notify.click();
                }, 100);
                </script>';

                header('location:' . SITEURL . 'admin/admin-manage.php');
            } else {
                //Cập nhật Admin không thành công
                $_SESSION['update'] = '<script>
                setTimeout(() => {
                    const notify = document.querySelector("#notify");
                    notify.onclick = () => {
                        showErrorToast("Cập nhật admin thất bại !");
                    }
                   notify.click();
                }, 100);
                </script>';

                header('location:' . SITEURL . 'admin/admin-manage.php');
            }
        }
    }
}
?>
</body>

</html>