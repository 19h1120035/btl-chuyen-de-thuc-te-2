<?php include('../config/constants.php'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet" />
    <!-- <link rel="stylesheet" href="./reset.css" /> -->
    <link rel="stylesheet" href="./css/login.css" />
    <link rel="stylesheet" href="./css/notify.css">
</head>

<body>
    <div id="notify"></div>
    <div id="toast"></div>
    <div class="signup">
        <div class="signup_content">
            <h1 class="signup_heading">Đăng Nhập</h1>
            <div class="signup-social">
                <div class="signup-social_item">
                    <i class="fab fa-google signup-social_icon"></i>
                    <span class="sign-social_text"> Đăng Nhập bằng Google</span>
                </div>
                <div class="signup-social_item">
                    <i class="fab fa-facebook signup-social_icon"></i>
                    <span class="sign-social_text"> Đăng Nhập bằng Facebook</span>
                </div>
            </div>
            <?php
        if (isset($_SESSION['login'])) {
            echo $_SESSION['login'];
            unset($_SESSION['login']);
        }

        if (isset($_SESSION['no-login-message'])) {
            echo $_SESSION['no-login-message'];
            unset($_SESSION['no-login-message']);
        }
        ?>

            <form action="" class="signup-form" method="POST">
                <div class="signup-form_group">
                    <label for="username" class="signup-form_label">Tên đăng nhập: </label>
                    <input type="text" class="signup-form_input" id="username" name="username" required placeholder="Tên đăng nhập" />
                </div>
                <div class="signup-form_group">
                    <label for="password" class="signup-form_label">Mật khẩu: </label>
                    <input type="password" class="signup-form_input" id="password" name="password" required  placeholder="Mật khẩu" />
                    <i class="fas fa-eye-slash"></i>
                </div>
                <input class="signup-form_submit" type="submit" name="submit" value="Đăng nhập">
            </form>
        </div>
    </div>
    <?php

//Kiểm tra xem nút Submit có được nhấp hay không 
if (isset($_POST['submit'])) {
    //Login
    // 1. Lấy dữ liệu từ form login 
    $username = $_POST['username'];
    $password = md5($_POST['password']);

    //2. SQL để kiểm tra xem người dùng có tên người dùng và mật khẩu có tồn tại hay không 
    $sql = "SELECT * FROM tbl_admin WHERE username='$username' AND password='$password'";

    //3. Thực thi truy vấn
    $res = mysqli_query($conn, $sql);

    //4. Đếm hàng để kiểm tra xem người dùng có tồn tại hay không 
    $count = mysqli_num_rows($res);
    if ($count == 1) {
        //OK
        $_SESSION['login'] = '<script>
        setTimeout(() => {
            const notify = document.querySelector("#notify");
            notify.onclick = () => {
                showSuccessToast("Đăng nhập thành công !");
            }
            notify.click();
        }, 100);
        </script>';
        $_SESSION['user'] = $username; //Để kiểm tra xem người dùng đã đăng nhập hay chưa và đăng xuất sẽ bỏ thiết lập nó 
        $_SESSION['password'] = $password;
        //chuyển hướng đến trang
        header('location:' . SITEURL . 'admin/');
    } else {
        //Người dùng không có sẵn và đăng nhập không có kết quả
        $_SESSION['login'] = '<script>
        setTimeout(() => {
            const notify = document.querySelector("#notify");
            notify.onclick = () => {
                showErrorToast("Tên đăng nhập hoặc mật khẩu không đúng !");
            }
            notify.click();
        }, 100);
        </script>';
        header('location:' . SITEURL . 'admin/login.php');
    }
}

?>
 <script>
        const pwField = document.querySelector(".signup-form_group input[type='password']")
        const toggleBtn = document.querySelector(".signup-form_group i")
        toggleBtn.onclick = () => {
            if (pwField.type == 'password') {
                pwField.type = "text";
                toggleBtn.classList.remove("fa-eye-slash");
                toggleBtn.classList.add("fa-eye");
            } else {
                pwField.type = "password";
                toggleBtn.classList.remove("fa-eye");
                toggleBtn.classList.add("fa-eye-slash");
            }
        }
    </script>
<script src="./js/notify.js"></script>
</body>

</html>