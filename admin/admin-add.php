    <?php
    include('./fronts/header.php');
    include('./fronts/navbar.php');
    ?>
    <?php
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        $error = array();
        if (empty($_POST['full_name'])) {
            $error['full_name'] = "Vui lòng nhập họ và tên";
        } else {
            $full_name = $_POST['full_name'];
            if (strlen($_POST['full_name']) < 2) {
                $error['full_name'] = "Họ và tên tối thiểu 2 kí tự";
                $full_name = "";
            } else {
                $full_name = $_POST['full_name'];
            }
        }

        if (empty($_POST['username'])) {
            $error['username'] = "Vui lòng nhập tên đăng nhập";
        } else {
            if (strlen($_POST['username']) < 2) {
                $error['username'] = "Tên đăng nhập tối thiểu 2 kí tự";
            } else {
                $username = $_POST['username'];
            }
        }
        if (empty($_POST['password'])) {
            $error['password'] = "Vui lòng nhập mật khẩu";
        } else {
            if (strlen($_POST['password']) < 6) {
                $error['password'] = "Mật khẩu tối thiểu 6 kí tự";
            } else {
                $password = $_POST['password'];
            }
        }
    }
    ?>
    <script>
        const navs = document.querySelectorAll('.sub-nav');
        navs[4].style.color = 'red';
    </script>
    <div class="add">
        <div class="form-add">
            <h1 class="title text-center">Thêm Quản Trị Viên</h1>
            <?php
            if (isset($_SESSION['add'])) //kiểm tra SESSION có được đặt hay không
            {
                echo $_SESSION['add']; //Hiển thị thông báo
                unset($_SESSION['add']); //Xóa thông báo
            }
            ?>
            <form action="" method="POST" enctype="multipart/form-data">
                <div class="form-item">
                    <label class="form-label" for="full_name">Họ và tên: </label>
                    <input class="form-input" type="text" id="full_name" name="full_name" required placeholder="VD: Nguyễn Văn A" value="<?php if (isset($full_name)) { echo $full_name; } ?>">
                    <?php
                    if (isset($error['full_name'])) {
                    ?>
                        <span style="color: red;"><?php echo $error['full_name']; ?></span>
                    <?php } ?>
                </div>
                <div class="form-item">
                    <label class="form-label" for="username">Tên đăng nhập: </label>
                    <input  class="form-input" type="text" id="username" name="username" placeholder="VD: admin" required value="<?php if (isset($username)) { echo $username; } ?>">
                    <script>
                        // function onInput() {
                        //     const id = document.getElementById('username').value;
                        //     if (id == "admin") {
                        //         document.getElementById('username').style.borderColor = "red";
                        //     } else {
                        //         document.getElementById('username').style.borderColor = "blue";
                        //     }
                        // }
                    </script>
                    <?php
                    if (isset($error['username'])) {
                    ?>
                        <span style="color: red;"><?php echo $error['username']; ?></span>
                    <?php } ?>
                </div>
                <div class=" form-item ">
                    <label class="form-label" for="password">Mật khẩu: </label>
                    <input class="form-input" type="password" id="password" name="password" required placeholder="Nhập mật khẩu" value="<?php if (isset($password)) { echo $password;  } ?>">
                    <?php
                    if (isset($error['password'])) {
                    ?>
                        <span style="color: red;"><?php echo $error['password']; ?></span>
                    <?php } ?>
                </div>
                <input type="submit" name="submit" value="Thêm" class="form-btn">
            </form>
            <?php
            // Xử lý Giá trị từ Biểu mẫu và Lưu nó trong Cơ sở dữ liệu
            if (isset($full_name) && isset($username) && isset($password)) {
                // Kiểm tra xem nút gửi có được nhấp hay không 
                if (!empty($full_name && $username && $password)) {
                    if (isset($_POST['submit'])) {

                        // 1. Lấy dữ liệu từ biểu mẫu 
                        $full_name;
                        $username;
                        $password = md5($password); //Mã hóa mật khẩu với MD5 

                        //2. Truy vấn SQL để lưu dữ liệu vào cơ sở dữ liệu 
                        $sql = "INSERT INTO tbl_admin SET full_name='$full_name', username='$username', password='$password'";

                        //3. Thực thi truy vấn và lưu dữ liệu vào cơ sở dữ liệu 
                        $res = mysqli_query($conn, $sql) or die(mysqli_error($conn));

                        //4. Kiểm tra xem dữ liệu (Truy vấn được thực thi) đã được chèn vào hay chưa và hiển thị thông báo thích hợp 
                        if ($res == TRUE) {
                            // hiển thị thông báo về về trang manage
                            $_SESSION['add'] = '<script>
                            setTimeout(() => {
                                const notify = document.querySelector("#notify");
                                notify.onclick = () => {
                                    showSuccessToast("Thêm admin thành công !");
                                }
                                   notify.click();
                            }, 100);
                            </script>';
                            header("location:" . SITEURL . 'admin/admin-manage.php');
                        } else {
                            // hiển thị thông báo về về trang manage
                            $_SESSION['add'] = '<script>
                            setTimeout(() => {
                                const notify = document.querySelector("#notify");
                                notify.onclick = () => {
                                    showErrorToast("Thêm admin thất bại !");
                                }
                                   notify.click();
                            }, 100);
                            </script>';
                            header("location:" . SITEURL . 'admin/admin-add.php');
                        }
                    }
                }
            }
            ?>
        </div>
    </div>
    </body>

    </html>