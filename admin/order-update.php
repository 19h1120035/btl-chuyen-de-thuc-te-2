    <?php
    include('./fronts/header.php');
    include('./fronts/navbar.php');
    ?>
    <script>
        const navs = document.querySelectorAll('.sub-nav');
        navs[3].style.color = 'red';
    </script>
    <div class="add">
        <div class="form-add">
            <h1 class="title text-center">Cập nhật đơn hàng</h1>
            <?php

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    //Truy vấn sql để lấy chi tiết đơn hàng
    $sql = "SELECT * FROM tbl_order WHERE id=$id";
    $res = mysqli_query($conn, $sql);
    $count = mysqli_num_rows($res);
    if ($count == 1) {
        $row = mysqli_fetch_assoc($res);
        $food = $row['food'];
        $price = $row['price'];
        $qty = $row['qty'];
        $status = $row['status'];
        $customer_name = $row['customer_name'];
        $customer_contact = $row['customer_contact'];
        $customer_email = $row['customer_email'];
        $customer_address = $row['customer_address'];
    } else {
        // header('location:' . SITEURL . 'admin/order-manage.php');
    }
} else {
    // header('location:' . SITEURL . 'admin/order-manage.php');
}

?>
            <form action="" method="POST" enctype="multipart/form-data">
                <div class="form-item">
                    <label class="form-label">Tên món ăn: </label>
                    <label class="font-18"><b><?php echo $food; ?></b></label>
                </div>
                <div class="form-item">
                    <label class="form-label">Giá tiền: </label>
                    <label class="font-18"><b><?php echo number_format($price,3 ,'.','.'); ?>đ</b></label>
                </div>
                <div class="form-item">
                    <label class="form-label" for="qty">Số lượng:</label>
                    <input class="form-input" type="number" name="qty" id="qty" min="1" value="<?php echo $qty; ?>">
                </div>
                <div class="form-item">
                    <label class="form-label" for="status">Tình trạng: </label>
                    <select class="form-input" name="status" id="status">
                        <option <?php if ($status == "Ordered") { echo "selected"; } ?> value="Ordered">Đã đặt hàng</option>
                        <option <?php if ($status == "On Delivery") { echo "selected"; } ?> value="On Delivery">Đang giao hàng</option>
                        <option <?php if ($status == "Delivered") { echo "selected"; } ?> value="Delivered" >Đã giao hàng</option>
                        <option <?php if ($status == "Cancelled") { echo "selected";} ?> value="Cancelled">Đã hủy</option>
                        </select>
                </div>
                <div class="form-item">
                    <label class="form-label" for="customer_name">Tên khách hàng : </label>
                    <input class="form-input" type="text" name="customer_name" id="customer_name" value="<?php echo $customer_name; ?>">
                </div>
                <div class="form-item">
                    <label class="form-label" for="customer_contact">Liên hệ: </label>
                    <input class="form-input" type="text" name="customer_contact" id="customer_contact" value="<?php echo $customer_contact; ?>">
                </div>
                <div class="form-item">
                    <label class="form-label" for="customer_email">Email: </label>
                    <input class="form-input" type="text" name="customer_email" id="customer_email" value="<?php echo $customer_email; ?>">
                </div>
                <div class="form-item">
                    <label class="form-label" for="customer_address">Địa chỉ: </label>
                    <textarea class="form-input" name="customer_address" id="customer_address" cols="30" rows="5"><?php echo $customer_address; ?></textarea>
                </div>
                <input type="hidden" name="id" value="<?php echo $id; ?>">
                <input type="hidden" name="price" value="<?php echo $price; ?>">
                <input type="submit" name="submit" value="Lưu" class="form-btn">
            </form>
            <?php
        //CHeck whether Update Button is Clicked or Not
        if (isset($_POST['submit'])) {
            //echo "Clicked";
            //Get All the Values from Form
            $id = $_POST['id'];
            $price = $_POST['price'];
            $qty = $_POST['qty'];

            $total = $price * $qty;

            $status = $_POST['status'];

            $customer_name = $_POST['customer_name'];
            $customer_contact = $_POST['customer_contact'];
            $customer_email = $_POST['customer_email'];
            $customer_address = $_POST['customer_address'];

            //Update the Values
            $sql2 = "UPDATE tbl_order SET 
                    qty = $qty,
                    total = $total,
                    status = '$status',
                    customer_name = '$customer_name',
                    customer_contact = '$customer_contact',
                    customer_email = '$customer_email',
                    customer_address = '$customer_address'
                    WHERE id=$id
                ";

            //Execute the Query
            $res2 = mysqli_query($conn, $sql2);

            //CHeck whether update or not
            //And REdirect to Manage Order with Message
            if ($res2 == true) {
                //Updated
                $_SESSION['update'] =  '<script>
                setTimeout(() => {
                    const notify = document.querySelector("#notify");
                    notify.onclick = () => {
                        showSuccessToast("Cập nhật đơn hàng thành công !");
                    }
                    notify.click();
                }, 100);
                </script>';
                header('location:' . SITEURL . 'admin/order-manage.php');

            } else {
                //Failed to Update
                $_SESSION['update'] = '<script>
                setTimeout(() => {
                    const notify = document.querySelector("#notify");
                    notify.onclick = () => {
                        showErrorToast("Cập nhật đơn hàng thất bại !");
                    }
                    notify.click();
                }, 100);
                </script>';
                header('location:' . SITEURL . 'admin/order-manage.php');
            }
        }
        ?>
        </div>
    </div>
</body>

</html>