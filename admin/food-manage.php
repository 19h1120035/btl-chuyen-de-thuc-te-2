<?php
include('./fronts/header.php');
include('./fronts/navbar.php');
if (isset($_GET['page'])) {
    $curPage = $_GET['page'];
}else {
    $curPage = 1;
}
?>
<script>
    const navs = document.querySelectorAll('.sub-nav');
    navs[2].style.color = 'red';
</script>
<div id="toast"></div>
<div class="manage">
    <div class="manage__head">
        <h2 class="title">Quản lý ẩm thực</h2>
        <a href="<?php echo SITEURL; ?>admin/food-add.php" class="btn">
            <i class="fas fa-plus-circle"></i>
            <span>Thêm ẩm thực</span>
        </a>
    </div>
    <?php
    if (isset($_SESSION['add'])) {
        echo $_SESSION['add'];
        unset($_SESSION['add']);
    }

    if (isset($_SESSION['delete'])) {
        echo $_SESSION['delete'];
        unset($_SESSION['delete']);
    }

    if (isset($_SESSION['upload'])) {
        echo $_SESSION['upload'];
        unset($_SESSION['upload']);
    }

    if (isset($_SESSION['unauthorize'])) {
        echo $_SESSION['unauthorize'];
        unset($_SESSION['unauthorize']);
    }

    if (isset($_SESSION['update'])) {
        echo $_SESSION['update'];
        unset($_SESSION['update']);
    }

    ?>
    <table class="manage__tbl">
        <tr>
            <th class="manage__tbl-th">#</th>
            <th class="manage__tbl-th">Tên món</th>
            <th class="manage__tbl-th">Giá</th>
            <th class="manage__tbl-th">Hình ảnh</th>
            <th class="manage__tbl-th">Đặc trưng</th>
            <th class="manage__tbl-th">Hoạt động</th>
            <th class="manage__tbl-th">Hành động</th>
        </tr>
        <?php
        /* =========Phân trang================ */
        $sql1 = "SELECT count(id) AS number FROM  tbl_food ";
        $result = pageResult($sql1);
        $number = 0;
        if ($result != null && count($result) > 0) {
            $number = $result[0]['number'];
        }
        $perPage = 6;
        $pages = ceil($number / $perPage);

        $current_page = 1;
        if (isset($_GET['page'])) {
            $current_page = $_GET['page'];
        }
        $index = ($current_page - 1) * $perPage;
        /* ========================================= */
        // Tạo truy vấn SQL để lấy tất cả món ăn
        $sql = "SELECT * FROM tbl_food LIMIT $index,$perPage";

        // Thực thi truy vấn
        $res = mysqli_query($conn, $sql);

        //Đếm hàng để kiểm tra xem có món ăn hay không
        $count = mysqli_num_rows($res);

        // Tạo biến seri-num và gán giá trị = 1
        $sn = $index + 1;

        if ($count > 0) {
            // Món ăn có trong database
            // Lấy món ăn từ database và hiển thị
            while ($row = mysqli_fetch_assoc($res)) {
                //Lấy các giá trị từ các cột riêng lẻ
                $id = $row['id'];
                $title = $row['title'];
                $price = $row['price'];
                $image_name = $row['image_name'];
                $featured = $row['featured'];
                $active = $row['active'];
        ?>
                <tr>
                    <td><?php echo $sn++; ?></td>
                    <td class="max-width"><?php echo $title; ?></td>
                    <td><?php echo number_format($price,3,'.','.')?>đ</td>
                    <td>
                        <?php
                        //Check xem có hình ảnh hay không
                        if ($image_name == "") {
                            //Không có hình ảnh, hiển thị thông báo lỗi
                            echo '<div class="error">Hình ảnh không được thêm vào.</div>';
                        } else {
                            //Có hình ảnh, hiển thị hình ảnh
                        ?>
                            <img class="img" src="<?php echo SITEURL; ?>/assets/img/food/<?php echo $image_name; ?>">
                    </td>
                <?php
                        }
                ?>
                <td>
                    <?php
                    if ($featured == "Yes") {
                        echo "Có";
                    } else {
                        echo "Không";
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if ($active == "Yes") {
                        echo "Có";
                    } else {
                        echo "Không";
                    }
                    ?>
                </td>
                <td>
                    <a href="<?php echo SITEURL; ?>admin/food-update.php?id=<?php echo $id; ?>" class="btn-edit"><i class="fas fa-edit"></i></a>
                    <a href="<?php echo SITEURL; ?>admin/food-delete.php?id=<?php echo $id; ?>&image_name=<?php echo $image_name; ?>" class="btn-delete"><i class="fas fa-trash-alt"></i></a>
                </td>
                </tr>
        <?php
            }
        } else {
            //Món ăn không được thêm vào database
            echo "<tr> <td colspan='7' class='error'> Món ăn chưa được thêm vào. </td> </tr>";
        }

        ?>
    </table>
    <div class="pagination">
    <ul>
        <li>
            <div class="page-prev"> <i class="fas fa-angle-left"></i></div>
        </li>
        <!-- <li><a class="page page-checked" href="#">1</a></li>
        <li><a class="page" href="#">2</a></li>
        <li><a class="page" href="#">3</a></li>
        <li><a class="page" href="#">4</a></li>
        <li><a class="page" href="#">5</a></li>
        <li><a class="page" href="#">6</a></li> -->
        <?php
        for ($i = 1; $i <= $pages; $i++) { ?>
            <li><a class="page <?php if ($i == $curPage) { echo 'page-checked';}?>" href="?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
            <?php
        } ?>
        <li>

            <div class="page-next"> <i class="fas fa-angle-right"></i></div>
        </li>
    </ul>
</div>
</div>

<script src="./js/main.js"></script>
<script src="./js/notify.js"></script>
</body>

</html>