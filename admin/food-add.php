    <?php
    include('./fronts/header.php');
    include('./fronts/navbar.php');
    ?>
    <script>
        const navs = document.querySelectorAll('.sub-nav');
        navs[2].style.color = 'red';
    </script>
    <div class="add">
        <div class="form-add">
            <h1 class="title text-center">Thêm Ẩm Thực</h1>
            <?php
            if (isset($_SESSION['upload'])) {
                echo $_SESSION['upload'];
                unset($_SESSION['upload']);
            }
            ?>
            <form action="" method="POST" enctype="multipart/form-data">
                <div class="form-item">
                    <label class="form-label" for="title">Tên món ăn: </label>
                    <input class="form-input" type="text" name="title" id="title" placeholder="Nhập tên món ăn" required>
                </div>
                <div class="form-item">
                    <label class="form-label" for="description">Mô tả: </label>
                    <textarea class="form-input" name="description" id="description" cols="30" rows="5" placeholder="Nhập mô tả món ăn" required></textarea>

                </div>
                <div class="form-item">
                    <label class="form-label" for="price">Giá tiền: </label>
                    <input class="form-input" type="number" name="price" id="price" min="1" placeholder="Nhập giá tiền" required>
                </div>
                <div class="form-item">
                    <label class="form-label" for="image">Chọn ảnh: </label>
                    <input class="form-file" type="file" id="image" name="image" required>
                </div>
                <div class="form-item">
                    <label class="form-label" for="category">Danh mục: </label>
                    <select class="form-input" name="category" id="category">
                        <?php
                        // Tạo mã PHP để hiển thị các danh mục từ Cơ sở dữ liệu
                        // 1. Tạo SQL để nhận tất cả các danh mục hoạt động từ cơ sở dữ liệu 
                        $sql = "SELECT * FROM tbl_category WHERE active='Yes'";
                        //Thực thi truy vấn 
                        $res = mysqli_query($conn, $sql);
                        //Đếm hàng để kiểm tra xem chúng tôi có danh mục hay không 
                        $count = mysqli_num_rows($res);
                        //
                        if ($count > 0) {
                            //Có danh mục
                            while ($row = mysqli_fetch_assoc($res)) {
                                //lấy thông tin chi tiết của các loại 
                                $id = $row['id'];
                                $title = $row['title'];
                        ?>
                                <option value="<?php echo $id; ?>"><?php echo $title; ?></option>
                            <?php
                            }
                        } else {
                            //Không có danh mục
                            ?>
                            <option value="0">Không tìm thấy danh mục </option>
                        <?php
                        }
                        //2. Hiển thị trên menu thả xuống 
                        ?>
                    </select>
                </div>
                <div class="form-item">
                    <label class="form-label" for="featured">Đặc trưng: </label>
                    <div class="form-check">
                        <input type="radio" name="featured" id="featured-yes" value="Yes"><label for="featured-yes">Có</label>
                        <input type="radio" name="featured" id="featured-no" value="No"><label for="featured-no">Không</label>
                    </div>
                </div>
                <div class="form-item">
                    <label class="form-label" for="active">Hoạt động: </label>
                    <div class="form-check">
                        <input type="radio" name="active" id="yes" value="Yes"><label for="yes">Có</label>
                        <input type="radio" name="active" id="no" value="No"><label for="no">Không</label>
                    </div>
                </div>
                <input type="submit" name="submit" value="Thêm" class="form-btn">
            </form>

            <?php

//Kiểm tra xem nút có được nhấp hay không 
if (isset($_POST['submit'])) {
    //Add the Food in Database
    //echo "Clicked";

    //1. Lấy dữ liệu từ form
    $title = $_POST['title'];
    $description = $_POST['description'];
    $price = $_POST['price'];
    $category = $_POST['category'];

    //Check nút radio có được chọn hay không
    if (isset($_POST['featured'])) {
        $featured = $_POST['featured'];
    } else {
        $featured = "No"; // Đặt giá trị mặc định là no
    }

    if (isset($_POST['active'])) {
        $active = $_POST['active'];
    } else {
        $active = "No"; // Đặt giá trị mặc định là no
    }

    // 2. Tải lên hình ảnh nếu được chọn
    // Kiểm tra xem hình ảnh đã chọn có được nhấp vào hay không và chỉ tải hình ảnh lên nếu hình ảnh được chọn 
    if (isset($_FILES['image']['name'])) {
        //Lấy thông tin chi tiết của hình ảnh được chọn
        $image_name = $_FILES['image']['name'];

        // Kiểm tra xem hình ảnh đã chọn có được nhấp vào hay không và chỉ tải hình ảnh lên nếu hình ảnh được chọn 
        if ($image_name != "") {
            // Hình ảnh được chọn
            //A. Đổi tên hình ảnh
            // Lấy phần mở rộng của hình ảnh đã chọn (jpg, png, gif, v.v.) 
            $ext = end(explode('.', $image_name));

            //Tạo tên mới cho hình ảnh 
            $image_name = "Food-Name-" . rand(0000, 9999) . '.' .$ext; // vd: "Food-Name-657.jpg"

            //B. Tải lên hình ảnh 
            // Lấy đường dẫn nguồn và đường dẫn đích 
            // Đường dẫn nguồn là vị trí hiện tại của hình ảnh 
            $src = $_FILES['image']['tmp_name'];

            //Đường dẫn đích cho hình ảnh được tải lên 
            $dst = "../assets/img/food/" . $image_name;

            //Cuối cùng uppload hình ảnh đồ ăn
            $upload = move_uploaded_file($src, $dst);

            //  kiểm tra xem hình ảnh có được tải lên không 
            if ($upload == false) {
                //Tải hình ảnh thất bại
                //Chuyển hướng đến add food với thông báo lỗi 
                $_SESSION['upload'] = '<script>
                setTimeout(() => {
                    const notify = document.querySelector("#notify");
                    notify.onclick = () => {
                        showErrorToast("Tải hình ảnh thất bại !");
                    }
                         notify.click();
                }, 100);
                </script>';
                header('location:' . SITEURL . 'admin/food-add.php');
                //dừng
                die();
            }
        }
    } else {
        $image_name = ""; //Đặt Giá trị mặc định là trống 
    }

    // 3. Chèn vào cơ sở dữ liệu

    // Tạo truy vấn SQL để lưu hoặc add food
    $sql2 = "INSERT INTO tbl_food SET title = '$title', description = '$description', price = $price, image_name = '$image_name', category_id = $category,  featured = '$featured', active = '$active' ";

    //Thực thi truy vấn
    $res2 = mysqli_query($conn, $sql2);

    // Kiểm tra xem dữ liệu đã được chèn vào hay chưa
    //4. Chuyển hướng với thông báo đến trang manage food
    if ($res2 == true) {
        //Đã chèn dữ liệu thành công 
        $_SESSION['add'] = '<script>
        setTimeout(() => {
            const notify = document.querySelector("#notify");
            notify.onclick = () => {
                showSuccessToast("Thêm món ăn thành công !");
            }
            notify.click();
        }, 100);
        </script>';
        header('location:' . SITEURL . 'admin/food-manage.php');
    } else {
        //Đã chèn dữ liệu thất bại
        $_SESSION['add'] = '<script>
        setTimeout(() => {
            const notify = document.querySelector("#notify");
            notify.onclick = () => {
                showErrorToast("Thêm món ăn thất bại !");
            }
            notify.click();
        }, 100);
        </script>';

        header('location:' . SITEURL . 'admin/food-manage.php');
    }
}

?>

        </div>
    </div>

    </body>

    </html>