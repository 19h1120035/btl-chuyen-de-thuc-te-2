<?php
include('../config/constants.php');

// Kiểm tra xem giá trị id và image_name đã được đặt hay chưa 
if (isset($_GET['id']) and isset($_GET['image_name'])) {
    // Lấy giá trị và xóa
    $id = $_GET['id'];
    $image_name = $_GET['image_name'];

    // Xóa tệp hình ảnh vật lý có sẵn 
    if ($image_name != "") {
        //hình ảnh có sẵn, remove
        $path = "../assets/img/category/" . $image_name;
        // ../assets/img/category/
        //Xóa hình ảnh
        $remove = unlink($path);

        //Nếu không thể xóa hình ảnh thì thêm thông báo lỗi và dừng chương trình 
        if ($remove == false) {
            //Đặt thông báo
            $_SESSION['remove'] = '<script>
            setTimeout(() => {
                const notify = document.querySelector("#notify");
                notify.onclick = () => {
                    showErrorToast("Không xóa được hình ảnh danh mục !");
                }
                   notify.click();
                }, 100);
            </script>';
            //Chuyển hướng về trang manage category
            header('location:' . SITEURL . 'admin/category-manage.php');
            //Dừng quá trình
            die();
        }
    }

    // Xóa dữ liệu khỏi cơ sở dữ liệu
    // Truy vấn SQL để xóa dữ liệu khỏi cơ sở dữ liệu
    $sql = "DELETE FROM tbl_category WHERE id=$id";

    //Thực thi truy vấn
    $res = mysqli_query($conn, $sql);

    //Kiểm tra xem dữ liệu có bị xóa khỏi cơ sở dữ liệu hay không 
    if ($res == true) {
        //Đặt thông báo về trang manage
        $_SESSION['delete'] = '<script>
        setTimeout(() => {
            const notify = document.querySelector("#notify");
            notify.onclick = () => {
                showSuccessToast("Đã xóa danh mục thành công !");
            }
            notify.click();
        }, 100);
        </script>';
        header('location:' . SITEURL . 'admin/category-manage.php');
    } else {
        //Đặt thông báo về trang manage
        $_SESSION['delete'] = '<script>
        setTimeout(() => {
            const notify = document.querySelector("#notify");
            notify.onclick = () => {
                showErrorToast("Xóa danh mục thất bại !");
            }
            notify.click();
        }, 100);
        </script>';
        header('location:' . SITEURL . 'admin/category-manage.php');
    }
} else {
    //chuyển hướng về trang
    header('location:' . SITEURL . 'admin/category-manage.php');
}
