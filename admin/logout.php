<?php 
    include('../config/constants.php');
    //1. Hủy 
    session_destroy(); //Hủy bỏ tất cả dữ liệu của phiên hiện tại

    //2. chuyển hướng đến trang login
    header('location:'.SITEURL.'admin/login.php');

?>