<?php
include('./fronts/header.php');
include('./fronts/navbar.php');
?>
<script>
    const navs = document.querySelectorAll('.sub-nav');
    navs[1].style.color = 'red';
</script>
<div class="add">
    <div class="form-add">
        <h1 class="title text-center">Cập nhật danh mục</h1>
        <?php

        // Kiểm tra xem id đã được đặt hay chưa 
        if (isset($_GET['id'])) {
            // Lấy ID và tất cả các chi tiết khác
            // echo "Lấy dữ liệu"; 
            $id = $_GET['id'];
            //Tạo Truy vấn SQL để nhận tất cả các chi tiết khác
            $sql = "SELECT * FROM tbl_category WHERE id=$id";

            // Thực thi truy vấn
            $res = mysqli_query($conn, $sql);

            //Đếm các hàng để kiểm tra xem id có hợp lệ hay không 

            $count = mysqli_num_rows($res);

            if ($count == 1) {
                //Nhận tất cả dữ liệu
                $row = mysqli_fetch_assoc($res);
                $title = $row['title'];
                $current_image = $row['image_name'];
                $featured = $row['featured'];
                $active = $row['active'];
            } else {
                //chuyển hướng đến trang manage admin với thông báo lỗi
                $_SESSION['no-category-found'] = '<script>
                setTimeout(() => {
                    const notify = document.querySelector("#notify");
                    notify.onclick = () => {
                        showErrorToast("Không tìm thấy danh mục !");
                    }
                    notify.click();
                }, 100);
                </script>';
                header('location:' . SITEURL . 'admin/category-manage.php');
            }
        } else {
            //chuyển hướng đến trang manage admin 
            header('location:' . SITEURL . 'admin/category-manage.php');
        }

        ?>
        <form action="" method="POST" enctype="multipart/form-data">
            <div class="form-item">
                <label class="form-label" for="title">Tên danh mục: </label>
                <input class="form-input" type="text" name="title" id="title" value="<?php echo $title; ?>" placeholder="Nhập tên danh mục">
            </div>
            <div class="form-item">
                <label class="form-label" for="current_image">Ảnh hiện tại: </label>
                <?php
                if ($current_image != "") {
                    // Hiển thị hình ảnh
                ?>
                    <img class="form-img" id="current_image" src="<?php echo SITEURL; ?>assets/img/category/<?php echo $current_image; ?>">
                <?php
                } else {
                    //Hiển thị thông báo
                    echo '<span style="color:red;">Hình ảnh không được thêm vào.</span>';
                }
                ?>
            </div>
            <div class="form-item">
                <label class="form-label" for="new_image">Hình ảnh mới: </label>
                <input class="form-file" type="file" name="image" id="new_image">
            </div>
            <div class="form-item">
                <label class="form-label" for="">Đặc trưng: </label>
                <div class="form-check">
                    <input <?php if ($featured == "Yes") {
                                echo "checked";
                            } ?> type="radio" name="featured" id="featured-yes" value="Yes"><label for="featured-yes">Có</label>
                    <input <?php if ($featured == "No") {
                                echo "checked";
                            } ?> type="radio" name="featured" id="featured-no" value="No"><label for="featured-no">Không</label>
                </div>
            </div>
            <div class="form-item">
                <label class="form-label" for="active">Hoạt động: </label>
                <div class="form-check">
                    <input <?php if ($active == "Yes") {
                                echo "checked";
                            } ?> type="radio" name="active" id="yes" value="Yes"><label for="yes">Có</label>
                    <input <?php if ($active == "No") {
                                echo "checked";
                            } ?> type="radio" name="active" id="no" value="No"><label for="no">Không</label>
                </div>
            </div>
            <input type="hidden" name="current_image" value="<?php echo $current_image; ?>">
            <input type="hidden" name="id" value="<?php echo $id; ?>">
            <input type="submit" name="submit" value="Lưu" class="form-btn">
        </form>
        <?php

if (isset($_POST['submit'])) {
    //1. Lấy các giá trị từ form
    $id = $_POST['id'];
    $title = $_POST['title'];
    $current_image = $_POST['current_image'];
    $featured = $_POST['featured'];
    $active = $_POST['active'];

    //2. Update ảnh mới nếu được chọn
    //Kiểm tra hình ảnh có được chọn hay không
    if (isset($_FILES['image']['name'])) {
        //Lây chi tiết hình ảnh
        $image_name = $_FILES['image']['name'];
        if ($image_name != "") {
            //A. Upload ảnh mới

            //Đổi tên ảnh 
            //Lấy phần mở rộng của hình ảnh (jpg, png, gif,...) vd: "cơm.jpg"
            $ext = end(explode('.', $image_name));

            //Đổi tên ảnh
            $image_name = "Food_Category_" . rand(000, 999) . '.' . $ext; // vd: Food_Category_834.jpg


            $source_path = $_FILES['image']['tmp_name'];

            $destination_path = "../assets/img/category/" . $image_name;

            //Finally Upload the Image
            $upload = move_uploaded_file($source_path, $destination_path);

            //Kiểm tra xem hình ảnh có được tải lên hay không
            //Nếu tải thất bại thì dừng và thông báo lỗi về trang manage
            if ($upload == false) {
                $_SESSION['upload'] = '<script>
                setTimeout(() => {
                    const notify = document.querySelector("#notify");
                    notify.onclick = () => {
                        showErrorToast("Không tải lên được hình ảnh !");
                    }
                    notify.click();
                }, 100);
                </script>';
                header('location:' . SITEURL . 'admin/category-manage.php');
                die();
            }

            //B. Xóa hình ảnh hiện tại nếu có
            if ($current_image != "") {
                $remove_path = "../assets/img/category/" . $current_image;

                $remove = unlink($remove_path);

                //Kiểm tra hình ảnh đã được xóa chưa
                //Nếu kh xóa được thì dừng và thông báo lỗi về trang manage
                if ($remove == false) {
                    $_SESSION['failed-remove'] = '<script>
                    setTimeout(() => {
                        const notify = document.querySelector("#notify");
                        notify.onclick = () => {
                            showErrorToast("Không xóa được hình ảnh hiện tại !");
                        }
                        notify.click();
                    }, 100);
                    </script>';
                    header('location:' . SITEURL . 'admin/category-manage.php');
                    die();
                }
            }
        } else {
            $image_name = $current_image;
        }
    } else {
        $image_name = $current_image;
    }

    //3. Update db
    $sql2 = "UPDATE tbl_category SET title = '$title', image_name = '$image_name', featured = '$featured', active = '$active'  WHERE id=$id ";

    //Thực thi câu truy vấn
    $res2 = mysqli_query($conn, $sql2);

    //4. Thông báo về trang manage
    //Kiểm tra xem có được thực thi không
    if ($res2 == true) {
        $_SESSION['update'] = '<script>
        setTimeout(() => {
            const notify = document.querySelector("#notify");
            notify.onclick = () => {
                showSuccessToast("Cập nhật danh mục thành công !");
            }
            notify.click();
        }, 100);
        </script>';
        header('location:' . SITEURL . 'admin/category-manage.php');
    } else {
        $_SESSION['update'] = '<script>
        setTimeout(() => {
            const notify = document.querySelector("#notify");
            notify.onclick = () => {
                showErrorToast("Cập nhật danh mục thất bại!");
            }
            notify.click();
        }, 100);
        </script>';
        header('location:' . SITEURL . 'admin/category-manage.php');
    }
}
?>
    </div>
</div>

</body>

</html>