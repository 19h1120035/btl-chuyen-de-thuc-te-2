var inputElement = document.querySelector('#status');
var all = document.getElementById('all');
var ordered = document.getElementById('ordered');
var onDelivery = document.getElementById('OnDelivery');
var Delivered = document.getElementById('Delivered');
var Cancelled = document.getElementById('Cancelled');
var page1 = document.getElementById('page1');
var page2 = document.getElementById('page2');
var page3 = document.getElementById('page3');
var page4 = document.getElementById('page4');
var page5 = document.getElementById('page5');
ordered.style.display = 'none';
onDelivery.style.display = 'none';
Delivered.style.display = 'none';
Cancelled.style.display = 'none';
page2.style.display = 'none';
page3.style.display = 'none';
page4.style.display = 'none';
page5.style.display = 'none';
inputElement.onchange = function(e) {
    if (e.target.value == "all") {
        all.style.display = 'flex';
        ordered.style.display = 'none';
        onDelivery.style.display = 'none';
        Delivered.style.display = 'none';
        Cancelled.style.display = 'none';
        page1.style.display = 'flex';
        page2.style.display = 'none';
        page3.style.display = 'none';
        page4.style.display = 'none';
        page5.style.display = 'none';
    } else if (e.target.value == "Ordered") {
        all.style.display = 'none';
        ordered.style.display = 'flex';
        onDelivery.style.display = 'none';
        Delivered.style.display = 'none';
        Cancelled.style.display = 'none';
        page1.style.display = 'none';
        page2.style.display = 'flex';
        page3.style.display = 'none';
        page4.style.display = 'none';
        page5.style.display = 'none';
    } else if (e.target.value == "On Delivery") {
        all.style.display = 'none';
        ordered.style.display = 'none';
        onDelivery.style.display = 'flex';
        Delivered.style.display = 'none';
        Cancelled.style.display = 'none';
        page1.style.display = 'none';
        page2.style.display = 'none';
        page3.style.display = 'flex';
        page4.style.display = 'none';
        page5.style.display = 'none';
    } else if (e.target.value == "Delivered") {
        all.style.display = 'none';
        ordered.style.display = 'none';
        onDelivery.style.display = 'none';
        Delivered.style.display = 'flex';
        Cancelled.style.display = 'none';
        page1.style.display = 'none';
        page2.style.display = 'none';
        page3.style.display = 'none';
        page4.style.display = 'flex';
        page5.style.display = 'none';
    } else if (e.target.value == "Cancelled") {
        all.style.display = 'none';
        ordered.style.display = 'none';
        onDelivery.style.display = 'none';
        Delivered.style.display = 'none';
        Cancelled.style.display = 'flex';
        page1.style.display = 'none';
        page2.style.display = 'none';
        page3.style.display = 'none';
        page4.style.display = 'none';
        page5.style.display = 'flex';
    }
}