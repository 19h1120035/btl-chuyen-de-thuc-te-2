const navLinks = document.querySelectorAll('.sub-nav');
navLinks.forEach(navLink => navLink.addEventListener('click', () => {
    navLinks.forEach(item => {
        item.style.color = '#008080';
    });
    navLink.style.color = 'red';
}));

// start phân trang
const pages = document.querySelectorAll(".page");

pages.forEach(page => page.onclick = function() {
    for (let i = 0; i < pages.length; i++) {
        const item = pages[i];
        // item.classList.remove("page-checked");
    }
});
const pagePrev = document.querySelector(".page-prev");
const pageNext = document.querySelector(".page-next");
var currentPage = 1;
pagePrev.onclick = function() {
    changePage(-1);
}

function changePage(number) {
    for (let i = 0; i < pages.length; i++) {
        if (pages[i].classList.contains("page-checked")) {
            currentPage = pages[i].textContent;
        }
    }
    if (number == 1) {
        if (currentPage < pages.length) {
            pages[currentPage - 1].classList.remove("page-checked");
            pages[currentPage].classList.add("page-checked");
            pages[currentPage].click();
        }
    } else if (number == -1) {
        if (currentPage > 1) {
            pages[currentPage - 1].classList.remove("page-checked");
            pages[currentPage - 2].classList.add("page-checked");
            pages[currentPage - 2].click();
        }

    }
}
pageNext.onclick = function() {
        changePage(1);
    }
    // end phân trang