<?php 
include('../config/constants.php');

    // 1. Lấy id của admin bị xóa
    $id = $_GET['id'];
    //2. Tạo  truy vấn SQL để xóa Admin     
    $sql = "DELETE FROM tbl_admin WHERE id=$id";
    // Thực thi truy vấn
    $res = mysqli_query($conn, $sql);
    // Kiểm tra xem truy vấn có được thực thi thành công hay không 
    if($res==true)
    {
        // Truy vấn được thực thi thành công và quản trị viên bị xóa
        //echo "Admin Deleted";
        //Tạo biến session để hiển thị thông báo
        $_SESSION['delete'] = '<script>
        setTimeout(() => {
            const notify = document.querySelector("#notify");
            notify.onclick = () => {
                showSuccessToast("Xóa admin thành công !");
            }
            notify.click();
        }, 100);
        </script>';
        // Chuyển hướng về trang manage admin
        header('location:'.SITEURL.'admin/admin-manage.php');
    }
    else
    {
        //Xóa admin thất bại
        $_SESSION['delete'] = '<script>
        setTimeout(() => {
            const notify = document.querySelector("#notify");
            notify.onclick = () => {
                showErrorToast("Xóa admin không thành công ! Vui lòng thử lại.");
            }
            notify.click();
        }, 100);
        </script>';
        header('location:'.SITEURL.'admin/admin-manage.php');
    }
