<?php 
    include('../config/constants.php');


    if(isset($_GET['id']) && isset($_GET['image_name'])) 
    {
        //Xử lý để xóa

        //1.  lấy id và tên hình ảnh
        $id = $_GET['id'];
        $image_name = $_GET['image_name'];

        // 2. Xóa hình ảnh nếu có
         // Kiểm tra xem hình ảnh có sẵn hay không và chỉ xóa nếu có 
        if($image_name != "")
        {
            // Nếu có hình ảnh và cần xóa khỏi thư mục
             // Lấy đường dẫn hình ảnh 
            $path = "../assets/img/food/".$image_name;

            //Xóa tệp hình ảnh khỏi thư mục 
            $remove = unlink($path);

            //Kiểm tra xem hình ảnh có bị xóa hay không 
            if($remove==false)
            {
                //Đặt thông báo về trang manage
                $_SESSION['upload'] = '<script>
                setTimeout(() => {
                    const notify = document.querySelector("#notify");
                    notify.onclick = () => {
                        showErrorToast("Xóa hình ảnh thất bại !");
                    }
                    notify.click();
                }, 100);
                </script>';
                header('location:'.SITEURL.'admin/food-manage.php');
                //Dừng
                die();
            }

        }

        //3. Xóa món ăn khỏi database
        $sql = "DELETE FROM tbl_food WHERE id=$id";
        //Thực thi truy vấn
        $res = mysqli_query($conn, $sql);

        // Kiểm tra xem truy vấn có được thực thi hay không và đặt thông báo để hiển thị
         //4. Chuyển hướng sang manage food và hiển thị thông báo 
        if($res==true)
        {
            //Xóa Ok 
            $_SESSION['delete'] = '<script>
            setTimeout(() => {
                const notify = document.querySelector("#notify");
                notify.onclick = () => {
                    showSuccessToast("Món ăn đã được xóa thành công !");
                }
                notify.click();
            }, 100);
            </script>';
            header('location:'.SITEURL.'admin/food-manage.php');
        }
        else
        {
            //Not OK
            $_SESSION['delete'] = '<script>
            setTimeout(() => {
                const notify = document.querySelector("#notify");
                notify.onclick = () => {
                    showErrorToast("Xóa món ăn thất bại !");
                }
                notify.click();
            }, 100);
            </script>';
            header('location:'.SITEURL.'admin/food-manage.php');
        }


    }
    else
    {
        //Chuyển hướng  về trang food-manage
        $_SESSION['unauthorize'] = '<script>
        setTimeout(() => {
            const notify = document.querySelector("#notify");
            notify.onclick = () => {
                showErrorToast("Truy cập không đúng !");
            }
            notify.click();
        }, 100);
        </script>';
        header('location:'.SITEURL.'admin/food-manage.php');
    }

?>