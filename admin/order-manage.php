    <?php
    include('./fronts/header.php');
    include('./fronts/navbar.php');
    ?>
    <script>
        const navs = document.querySelectorAll('.sub-nav');
        navs[3].style.color = 'red';
    </script>
    <?php
        if (isset($_GET['page'])) {
            $curPage = $_GET['page'];
        }else {
            $curPage =1;
        }
    ?>
    <div id="toast"></div>
    <div class="manage">
        <div class="manage__head">
            <h2 class="title">Quản lý đơn hàng</h2>
            <div class="status">
                <label class="" for="status">Tình trạng: </label>
                <select class="" name="status" id="status">
                    <option id = "stt1" value="all" selected>Tất cả đơn hàng</option>
                    <option id = "stt2" value="Ordered">Đã đặt hàng</option>
                    <option id = "stt3" value="On Delivery">Đang giao hàng</option>
                    <option id = "stt4" value="Delivered">Đã giao hàng</option>
                    <option id = "stt5" value="Cancelled">Đã hủy</option>
                </select>
            </div>
        </div>
        <?php
        if (isset($_SESSION['update'])) {
            echo $_SESSION['update'];
            unset($_SESSION['update']);
        }
        ?>
        <table id="all" class="manage__tbl">
            <tr>
                <th class="manage__tbl-th font-18">#</th>
                <th class="manage__tbl-th font-18">Ngày đặt hàng</th>
                <th class="manage__tbl-th font-18">Món ăn</th>
                <th class="manage__tbl-th font-18">Giá</th>
                <th class="manage__tbl-th font-18">Số lượng</th>
                <th class="manage__tbl-th font-18">Tổng</th>
                <th class="manage__tbl-th font-18">Tình trạng</th>
                <th class="manage__tbl-th font-18">Khách hàng</th>
                <th class="manage__tbl-th font-18">Số điện thoại</th>
                <th class="manage__tbl-th font-18">Email</th>
                <th class="manage__tbl-th font-18">Địa chỉ</th>
                <th class="manage__tbl-th font-18">Hành động</th>
            </tr>
            <?php
            /* =========Phân trang================ */
            $sql1 = "SELECT count(id) AS number FROM  tbl_order ";
            $result = pageResult($sql1);
            $number = 0;
            if ($result != null && count($result) > 0) {
                $number = $result[0]['number'];
            }
            $perPage = 6;
            $pages = ceil($number /  $perPage);

            $current_page = 1;
            if (isset($_GET['page'])) {
                $current_page = $_GET['page'];
            }
            $index = ($current_page - 1) *  $perPage;
            /* ========================================= */
            // Lấy tất cả các đơn hàng từ cơ sở dữ liệu 
            $sql = "SELECT * FROM tbl_order ORDER BY id DESC LIMIT $index, $perPage"; // hiển thị đơn đặt hàng mới nhất lúc đầu 
            //Thực thi truy vấn
            $res = mysqli_query($conn, $sql);
            //đếm hàng
            $count = mysqli_num_rows($res);
            $sn = $index + 1; //tạo biến serinum và gán giá trị
            if ($count > 0) {
                //Đơn hàng có sẵn
                while ($row = mysqli_fetch_assoc($res)) {
                    // Nhận tất cả các chi tiết đặt hàng
                    $id = $row['id'];
                    $food = $row['food'];
                    $price = $row['price'];
                    $qty = $row['qty'];
                    $total = $row['total'];
                    $order_date = $row['order_date'];
                    $status = $row['status'];
                    $customer_name = $row['customer_name'];
                    $customer_contact = $row['customer_contact'];
                    $customer_email = $row['customer_email'];
                    $customer_address = $row['customer_address'];
            ?>
                    <tr>
                        <td class="font-14"><?php echo $sn++; ?></td>
                        <td class="font-14"><?php echo $order_date; ?></td>
                        <td class="font-14"><?php echo $food; ?></td>
                        <td class="font-14"><?php echo number_format($price,3,'.','.'); ?>đ</td>
                        <td class="font-14"><?php echo $qty; ?></td>
                        <td class="font-14"><?php echo number_format($total,3,'.','.'); ?>đ</td>
                        <td class="font-14">
                            <?php
                            // Đã đặt hàng, Khi giao hàng, Đã giao, Đã hủy 
                            if ($status == "Ordered") {
                                echo "<label style='color: blue;'>Đã đặt hàng</label>";
                            } elseif ($status == "On Delivery") {
                                echo "<label style='color: orange;'>Đang giao hàng</label>";
                            } elseif ($status == "Delivered") {
                                echo "<label style='color: green;'><b>Đã giao hàng</b></label>";
                            } elseif ($status == "Cancelled") {
                                echo "<label style='color: red;'>Đã hủy</label>";
                            }
                            ?>
                        </td>
                        <td class="font-14"><?php echo $customer_name; ?></td>
                        <td class="font-14"><?php echo $customer_contact; ?></td>
                        <td class="font-14">
                        <?php
                            if (strlen($customer_email) >= 16) {
                                $pos = strpos($customer_email, '@');
                                $email = substr_replace($customer_email, ' ', $pos, 0);
                                echo $email;
                            }
                            else {
                                echo $customer_email;
                            }
                            ?>
                        </td>
                        <td class="font-14"><?php echo $customer_address; ?></td>
                        <td class="font-14">
                            <a href="<?php echo SITEURL; ?>admin/order-update.php?id=<?php echo $id; ?>" class="btn-edit"><i class="fas fa-edit"></i></a>
                        </td>
                    </tr>
            <?php
                }
            } else {
                //Đơn hàng không có sẵn
                echo "<tr><td colspan='12' class='error'>Đơn đặt hàng không có sẵn </td></tr>";
            }
            ?>
        </table>
        <div id="page1" class="pagination">
            <ul>
                <li>
                    <div class="page-prev"> <i class="fas fa-angle-left"></i></div>
                </li>
                <?php
        for ($i = 1; $i <= $pages; $i++) { ?>
            <li><a class="page <?php if ($i == $curPage) { echo 'page-checked';}?>" href="?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
            <?php
        } ?>
                <li>
                    <div class="page-next"> <i class="fas fa-angle-right"></i></div>
                </li>
            </ul>
        </div>
        <table id="ordered" class="manage__tbl">
            <tr>
                <th class="manage__tbl-th font-18">#</th>
                <th class="manage__tbl-th font-18">Ngày đặt hàng</th>
                <th class="manage__tbl-th font-18">Món ăn</th>
                <th class="manage__tbl-th font-18">Giá</th>
                <th class="manage__tbl-th font-18">Số lượng</th>
                <th class="manage__tbl-th font-18">Tổng</th>
                <th class="manage__tbl-th font-18">Tình trạng</th>
                <th class="manage__tbl-th font-18">Khách hàng</th>
                <th class="manage__tbl-th font-18">Số điện thoại</th>
                <th class="manage__tbl-th font-18">Email</th>
                <th class="manage__tbl-th font-18">Địa chỉ</th>
                <th class="manage__tbl-th font-18">Hành động</th>
            </tr>
            <?php
            /* =========Phân trang================ */
            $sql1 = "SELECT count(id) AS number FROM  tbl_order WHERE status='Ordered' ";
            $result = pageResult($sql1);
            $number = 0;
            if ($result != null && count($result) > 0) {
                $number = $result[0]['number'];
            }
            $pages = ceil($number / 6);

            $current_page = 1;
            if (isset($_GET['page'])) {
                $current_page = $_GET['page'];
            }
            $index = ($current_page - 1) * 6;
            /* ========================================= */
            // Lấy tất cả các đơn hàng từ cơ sở dữ liệu 
            $sql = "SELECT * FROM tbl_order WHERE status='Ordered' ORDER BY id DESC LIMIT $index,6"; // hiển thị đơn đặt hàng mới nhất lúc đầu 
            //Thực thi truy vấn
            $res = mysqli_query($conn, $sql);
            //đếm hàng
            $count = mysqli_num_rows($res);
            $sn = $index + 1; //tạo biến serinum và gán giá trị
            if ($count > 0) {
                //Đơn hàng có sẵn
                while ($row = mysqli_fetch_assoc($res)) {
                    // Nhận tất cả các chi tiết đặt hàng
                    $id = $row['id'];
                    $food = $row['food'];
                    $price = $row['price'];
                    $qty = $row['qty'];
                    $total = $row['total'];
                    $order_date = $row['order_date'];
                    $status = $row['status'];
                    $customer_name = $row['customer_name'];
                    $customer_contact = $row['customer_contact'];
                    $customer_email = $row['customer_email'];
                    $customer_address = $row['customer_address'];
            ?>
                    <tr>
                        <td class="font-14"><?php echo $sn++; ?></td>
                        <td class="font-14"><?php echo $order_date; ?></td>
                        <td class="font-14"><?php echo $food; ?></td>
                        <td class="font-14"><?php echo number_format($price,3,'.','.'); ?>đ</td>
                        <td class="font-14"><?php echo $qty; ?></td>
                        <td class="font-14"><?php echo number_format($total,3,'.','.'); ?>đ</td>
                        <td class="font-14">
                            <?php
                            // Đã đặt hàng, Khi giao hàng, Đã giao, Đã hủy 
                            if ($status == "Ordered") {
                                echo "<label style='color: blue;'>Đã đặt hàng</label>";
                            } elseif ($status == "On Delivery") {
                                echo "<label style='color: orange;'>Đang giao hàng</label>";
                            } elseif ($status == "Delivered") {
                                echo "<label style='color: green;'><b>Đã giao hàng</b></label>";
                            } elseif ($status == "Cancelled") {
                                echo "<label style='color: red;'>Đã hủy</label>";
                            }
                            ?>
                        </td>
                        <td class="font-14"><?php echo $customer_name; ?></td>
                        <td class="font-14"><?php echo $customer_contact; ?></td>
                        <td class="font-14">
                        <?php
                            if (strlen($customer_email) >= 16) {
                                $pos = strpos($customer_email, '@');
                                $email = substr_replace($customer_email, ' ', $pos, 0);
                                echo $email;
                            }
                            else {
                                echo $customer_email;
                            }
                            ?>
                        </td>
                        <td class="font-14"><?php echo $customer_address; ?></td>
                        <td class="font-14">
                            <a href="<?php echo SITEURL; ?>admin/order-update.php?id=<?php echo $id; ?>" class="btn-edit"><i class="fas fa-edit"></i></a>
                        </td>
                    </tr>
            <?php
                }
            } else {
                //Đơn hàng không có sẵn
                echo "<tr><td colspan='12' class='error'>Đơn đặt hàng không có sẵn </td></tr>";
            }
            ?>
        </table>
        <div id="page2" class="pagination">
            <ul>
                <li>
                    <div class="page-prev"> <i class="fas fa-angle-left"></i></div>
                </li>
                <?php
        for ($i = 1; $i <= $pages; $i++) { ?>
            <li><a class="page <?php if ($i == $curPage) { echo 'page-checked';}?>" href="?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
            <?php
        } ?>
                <li>

                    <div class="page-next"> <i class="fas fa-angle-right"></i></div>
                </li>
            </ul>
        </div>
        <table id="OnDelivery" class="manage__tbl">
            <tr>
                <th class="manage__tbl-th font-18">#</th>
                <th class="manage__tbl-th font-18">Ngày đặt hàng</th>
                <th class="manage__tbl-th font-18">Món ăn</th>
                <th class="manage__tbl-th font-18">Giá</th>
                <th class="manage__tbl-th font-18">Số lượng</th>
                <th class="manage__tbl-th font-18">Tổng</th>
                <th class="manage__tbl-th font-18">Tình trạng</th>
                <th class="manage__tbl-th font-18">Khách hàng</th>
                <th class="manage__tbl-th font-18">Số điện thoại</th>
                <th class="manage__tbl-th font-18">Email</th>
                <th class="manage__tbl-th font-18">Địa chỉ</th>
                <th class="manage__tbl-th font-18">Hành động</th>
            </tr>
            <?php
            /* =========Phân trang================ */
            $sql1 = "SELECT count(id) AS number FROM  tbl_order WHERE status='On Delivery' ";
            $result = pageResult($sql1);
            $number = 0;
            if ($result != null && count($result) > 0) {
                $number = $result[0]['number'];
            }
            $pages = ceil($number / 6);

            $current_page = 1;
            if (isset($_GET['page'])) {
                $current_page = $_GET['page'];
            }
            $index = ($current_page - 1) * 6;
            /* ========================================= */
            // Lấy tất cả các đơn hàng từ cơ sở dữ liệu 
            $sql = "SELECT * FROM tbl_order WHERE status='On Delivery' ORDER BY id DESC LIMIT $index,6"; // hiển thị đơn đặt hàng mới nhất lúc đầu 
            //Thực thi truy vấn
            $res = mysqli_query($conn, $sql);
            //đếm hàng
            $count = mysqli_num_rows($res);
            $sn = $index + 1; //tạo biến serinum và gán giá trị
            if ($count > 0) {
                //Đơn hàng có sẵn
                while ($row = mysqli_fetch_assoc($res)) {
                    // Nhận tất cả các chi tiết đặt hàng
                    $id = $row['id'];
                    $food = $row['food'];
                    $price = $row['price'];
                    $qty = $row['qty'];
                    $total = $row['total'];
                    $order_date = $row['order_date'];
                    $status = $row['status'];
                    $customer_name = $row['customer_name'];
                    $customer_contact = $row['customer_contact'];
                    $customer_email = $row['customer_email'];
                    $customer_address = $row['customer_address'];
            ?>
                    <tr>
                        <td class="font-14"><?php echo $sn++; ?></td>
                        <td class="font-14"><?php echo $order_date; ?></td>
                        <td class="font-14"><?php echo $food; ?></td>
                        <td class="font-14"><?php echo number_format($price,3,'.','.'); ?>đ</td>
                        <td class="font-14"><?php echo $qty; ?></td>
                        <td class="font-14"><?php echo number_format($total,3,'.','.'); ?>đ</td>
                        <td class="font-14">
                            <?php
                            // Đã đặt hàng, Khi giao hàng, Đã giao, Đã hủy 
                            if ($status == "Ordered") {
                                echo "<label style='color: blue;'>Đã đặt hàng</label>";
                            } elseif ($status == "On Delivery") {
                                echo "<label style='color: orange;'>Đang giao hàng</label>";
                            } elseif ($status == "Delivered") {
                                echo "<label style='color: green;'><b>Đã giao hàng</b></label>";
                            } elseif ($status == "Cancelled") {
                                echo "<label style='color: red;'>Đã hủy</label>";
                            }
                            ?>
                        </td>
                        <td class="font-14"><?php echo $customer_name; ?></td>
                        <td class="font-14"><?php echo $customer_contact; ?></td>
                        <td class="font-14">
                        <?php
                            if (strlen($customer_email) >= 16) {
                                $pos = strpos($customer_email, '@');
                                $email = substr_replace($customer_email, ' ', $pos, 0);
                                echo $email;
                            }
                            else {
                                echo $customer_email;
                            }
                            ?>
                        </td>
                        <td class="font-14"><?php echo $customer_address; ?></td>
                        <td class="font-14">
                            <a href="<?php echo SITEURL; ?>admin/order-update.php?id=<?php echo $id; ?>" class="btn-edit"><i class="fas fa-edit"></i></a>
                        </td>
                    </tr>
            <?php
                }
            } else {
                //Đơn hàng không có sẵn
                echo "<tr><td colspan='12' class='error'>Đơn đặt hàng không có sẵn </td></tr>";
            }
            ?>
        </table>
        <div id="page3" class="pagination">
            <ul>
                <li>
                    <div class="page-prev"> <i class="fas fa-angle-left"></i></div>
                </li>
                <?php
        for ($i = 1; $i <= $pages; $i++) { ?>
            <li><a class="page <?php if ($i == $curPage) { echo 'page-checked';}?>" href="?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
            <?php
        } ?>
                <li>
                    <div class="page-next"> <i class="fas fa-angle-right"></i></div>
                </li>
            </ul>
        </div>
        <table id="Delivered" class="manage__tbl">
            <tr>
                <th class="manage__tbl-th font-18">#</th>
                <th class="manage__tbl-th font-18">Ngày đặt hàng</th>
                <th class="manage__tbl-th font-18">Món ăn</th>
                <th class="manage__tbl-th font-18">Giá</th>
                <th class="manage__tbl-th font-18">Số lượng</th>
                <th class="manage__tbl-th font-18">Tổng</th>
                <th class="manage__tbl-th font-18">Tình trạng</th>
                <th class="manage__tbl-th font-18">Khách hàng</th>
                <th class="manage__tbl-th font-18">Số điện thoại</th>
                <th class="manage__tbl-th font-18">Email</th>
                <th class="manage__tbl-th font-18">Địa chỉ</th>
                <th class="manage__tbl-th font-18">Hành động</th>
            </tr>
            <?php
            /* =========Phân trang================ */
            $sql1 = "SELECT count(id) AS number FROM  tbl_order WHERE status='Delivered' ";
            $result = pageResult($sql1);
            $number = 0;
            if ($result != null && count($result) > 0) {
                $number = $result[0]['number'];
            }
            $pages = ceil($number / 6);

            $current_page = 1;
            if (isset($_GET['page'])) {
                $current_page = $_GET['page'];
            }
            $index = ($current_page - 1) * 6;
            /* ========================================= */
            // Lấy tất cả các đơn hàng từ cơ sở dữ liệu 
            $sql = "SELECT * FROM tbl_order WHERE status='Delivered' ORDER BY id DESC LIMIT $index,6"; // hiển thị đơn đặt hàng mới nhất lúc đầu 
            //Thực thi truy vấn
            $res = mysqli_query($conn, $sql);
            //đếm hàng
            $count = mysqli_num_rows($res);
            $sn = $index + 1; //tạo biến serinum và gán giá trị
            if ($count > 0) {
                //Đơn hàng có sẵn
                while ($row = mysqli_fetch_assoc($res)) {
                    // Nhận tất cả các chi tiết đặt hàng
                    $id = $row['id'];
                    $food = $row['food'];
                    $price = $row['price'];
                    $qty = $row['qty'];
                    $total = $row['total'];
                    $order_date = $row['order_date'];
                    $status = $row['status'];
                    $customer_name = $row['customer_name'];
                    $customer_contact = $row['customer_contact'];
                    $customer_email = $row['customer_email'];
                    $customer_address = $row['customer_address'];
            ?>
                    <tr>
                        <td class="font-14"><?php echo $sn++; ?></td>
                        <td class="font-14"><?php echo $order_date; ?></td>
                        <td class="font-14"><?php echo $food; ?></td>
                        <td class="font-14"><?php echo number_format($price,3,'.','.'); ?>đ</td>
                        <td class="font-14"><?php echo $qty; ?></td>
                        <td class="font-14"><?php echo number_format($total,3,'.','.'); ?>đ</td>
                        <td class="font-14">
                            <?php
                            // Đã đặt hàng, Khi giao hàng, Đã giao, Đã hủy 
                            if ($status == "Ordered") {
                                echo "<label style='color: blue;'>Đã đặt hàng</label>";
                            } elseif ($status == "On Delivery") {
                                echo "<label style='color: orange;'>Đang giao hàng</label>";
                            } elseif ($status == "Delivered") {
                                echo "<label style='color: green;'><b>Đã giao hàng</b></label>";
                            } elseif ($status == "Cancelled") {
                                echo "<label style='color: red;'>Đã hủy</label>";
                            }
                            ?>
                        </td>
                        <td class="font-14"><?php echo $customer_name; ?></td>
                        <td class="font-14"><?php echo $customer_contact; ?></td>
                        <td class="font-14">
                        <?php
                            if (strlen($customer_email) >= 16) {
                                $pos = strpos($customer_email, '@');
                                $email = substr_replace($customer_email, ' ', $pos, 0);
                                echo $email;
                            }
                            else {
                                echo $customer_email;
                            }
                            ?>
                        </td>
                        <td class="font-14"><?php echo $customer_address; ?></td>
                        <td class="font-14">
                            <a href="<?php echo SITEURL; ?>admin/order-update.php?id=<?php echo $id; ?>" class="btn-edit"><i class="fas fa-edit"></i></a>
                        </td>
                    </tr>
            <?php
                }
            } else {
                //Đơn hàng không có sẵn
                echo "<tr><td colspan='12' class='error'>Đơn đặt hàng không có sẵn </td></tr>";
            }
            ?>
        </table>
        <div id="page4" class="pagination">
            <ul>
                <li>
                    <div class="page-prev"> <i class="fas fa-angle-left"></i></div>
                </li>
                <?php
        for ($i = 1; $i <= $pages; $i++) { ?>
            <li><a class="page <?php if ($i == $curPage) { echo 'page-checked';}?>" href="?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
            <?php
        } ?>
                <li>
                    <div class="page-next"> <i class="fas fa-angle-right"></i></div>
                </li>
            </ul>
        </div>
        <table id="Cancelled" class="manage__tbl">
            <tr>
                <th class="manage__tbl-th font-18">#</th>
                <th class="manage__tbl-th font-18">Ngày đặt hàng</th>
                <th class="manage__tbl-th font-18">Món ăn</th>
                <th class="manage__tbl-th font-18">Giá</th>
                <th class="manage__tbl-th font-18">Số lượng</th>
                <th class="manage__tbl-th font-18">Tổng</th>
                <th class="manage__tbl-th font-18">Tình trạng</th>
                <th class="manage__tbl-th font-18">Khách hàng</th>
                <th class="manage__tbl-th font-18">Số điện thoại</th>
                <th class="manage__tbl-th font-18">Email</th>
                <th class="manage__tbl-th font-18">Địa chỉ</th>
                <th class="manage__tbl-th font-18">Hành động</th>
            </tr>
            <?php
            /* =========Phân trang================ */
            $sql1 = "SELECT count(id) AS number FROM  tbl_order WHERE status='Cancelled' ";
            $result = pageResult($sql1);
            $number = 0;
            if ($result != null && count($result) > 0) {
                $number = $result[0]['number'];
            }
            $pages = ceil($number / 6);

            $current_page = 1;
            if (isset($_GET['page'])) {
                $current_page = $_GET['page'];
            }
            $index = ($current_page - 1) * 6;
            /* ========================================= */
            // Lấy tất cả các đơn hàng từ cơ sở dữ liệu 
            $sql = "SELECT * FROM tbl_order WHERE status='Cancelled' ORDER BY id DESC LIMIT $index,6"; // hiển thị đơn đặt hàng mới nhất lúc đầu 
            //Thực thi truy vấn
            $res = mysqli_query($conn, $sql);
            //đếm hàng
            $count = mysqli_num_rows($res);
            $sn = $index + 1; //tạo biến serinum và gán giá trị
            if ($count > 0) {
                //Đơn hàng có sẵn
                while ($row = mysqli_fetch_assoc($res)) {
                    // Nhận tất cả các chi tiết đặt hàng
                    $id = $row['id'];
                    $food = $row['food'];
                    $price = $row['price'];
                    $qty = $row['qty'];
                    $total = $row['total'];
                    $order_date = $row['order_date'];
                    $status = $row['status'];
                    $customer_name = $row['customer_name'];
                    $customer_contact = $row['customer_contact'];
                    $customer_email = $row['customer_email'];
                    $customer_address = $row['customer_address'];
            ?>
                    <tr>
                        <td class="font-14"><?php echo $sn++; ?></td>
                        <td class="font-14"><?php echo $order_date; ?></td>
                        <td class="font-14"><?php echo $food; ?></td>
                        <td class="font-14"><?php echo number_format($price,3,'.','.'); ?>đ</td>
                        <td class="font-14"><?php echo $qty; ?></td>
                        <td class="font-14"><?php echo number_format($total,3,'.','.'); ?>đ</td>
                        <td class="font-14">
                            <?php
                            // Đã đặt hàng, Khi giao hàng, Đã giao, Đã hủy 
                            if ($status == "Ordered") {
                                echo "<label style='color: blue;'>Đã đặt hàng</label>";
                            } elseif ($status == "On Delivery") {
                                echo "<label style='color: orange;'>Đang giao hàng</label>";
                            } elseif ($status == "Delivered") {
                                echo "<label style='color: green;'><b>Đã giao hàng</b></label>";
                            } elseif ($status == "Cancelled") {
                                echo "<label style='color: red;'>Đã hủy</label>";
                            }
                            ?>
                        </td>
                        <td class="font-14"><?php echo $customer_name; ?></td>
                        <td class="font-14"><?php echo $customer_contact; ?></td>
                        <td class="font-14">
                        <?php
                            if (strlen($customer_email) >= 16) {
                                $pos = strpos($customer_email, '@');
                                $email = substr_replace($customer_email, ' ', $pos, 0);
                                echo $email;
                            }
                            else {
                                echo $customer_email;
                            }
                            ?>
                        </td>
                        <td class="font-14"><?php echo $customer_address; ?></td>
                        <td class="font-14">
                            <a href="<?php echo SITEURL; ?>admin/order-update.php?id=<?php echo $id; ?>" class="btn-edit"><i class="fas fa-edit"></i></a>
                        </td>
                    </tr>
            <?php
                }
            } else {
                //Đơn hàng không có sẵn
                echo "<tr><td colspan='12' class='error'>Đơn đặt hàng không có sẵn </td></tr>";
            }
            ?>
        </table>
        <div id="page5" class="pagination">
            <ul>
                <li>
                    <div class="page-prev"> <i class="fas fa-angle-left"></i></div>
                </li>
                <?php
        for ($i = 1; $i <= $pages; $i++) { ?>
            <li><a class="page <?php if ($i == $curPage) { echo 'page-checked';}?>" href="?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
            <?php
        } ?>
                <li>
                    <div class="page-next"> <i class="fas fa-angle-right"></i></div>
                </li>
            </ul>
        </div>
    </div>
    <script>
        var pas = document.querySelectorAll('.page');
        // if (pas[3]') {
        //     window.onload = pas[3].classList.add('page-checked');
        // }
        // alert(pas[3].value);
    </script>
    <script src="./js/main.js"></script>
    <script src="./js/notify.js"></script>
    <script src="./js/order-change.js"></script>
    </body>

    </html>