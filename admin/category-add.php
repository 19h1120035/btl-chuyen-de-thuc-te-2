   <?php
    include('./fronts/header.php');
    include('./fronts/navbar.php');
    ?>
    <script>
        const navs = document.querySelectorAll('.sub-nav');
        navs[1].style.color = 'red';
    </script>
    <div class="add">
        <div class="form-add">
            <h1 class="title text-center">Thêm danh mục</h1>
            <?php

if (isset($_SESSION['add'])) {
    echo $_SESSION['add'];
    unset($_SESSION['add']);
}

if (isset($_SESSION['upload'])) {
    echo $_SESSION['upload'];
    unset($_SESSION['upload']);
}

?>
            <form action="" method="POST" enctype="multipart/form-data">
                <div class="form-item">
                    <label class="form-label" for="title">Tiêu đề: </label>
                    <input class="form-input" type="text" name="title" id="title" placeholder="Nhập tiêu đề danh mục" required>
                </div>
                <div class="form-item">
                    <label class="form-label" for="image">Chọn ảnh: </label>
                    <input class="form-file" type="file" id="image" name="image" required>
                </div>
                <div class="form-item">
                    <label class="form-label" for="featured">Đặc trưng: </label>
                    <div class="form-check">
                        <input type="radio" name="featured" id="featured-yes" value="Yes"><label for="featured-yes">Có</label>
                        <input type="radio" name="featured" id="featured-no" value="No"><label for="featured-no">Không</label>
                    </div>
                </div>
                <div class="form-item">
                    <label class="form-label" for="active">Hoạt động: </label>
                    <div class="form-check">
                        <input type="radio" name="active" id="yes" value="Yes"><label for="yes">Có</label>
                        <input type="radio" name="active" id="no" value="No"><label for="no">Không</label>
                    </div>
                </div>
                <input type="submit" name="submit" value="Thêm" class="form-btn">
            </form>
            <?php

//Kiểm tra xem nút gửi có được nhấp hay không 
if (isset($_POST['submit'])) {

    //1. Nhận giá trị từ form danh mục
    $title = $_POST['title'];
    //Đối với đầu vào radio, check xem nút đã được chọn hay chưa
    if (isset($_POST['featured'])) {
        //Nhận giá trị từ form
        $featured = $_POST['featured'];
    } else {
        //Nếu chưa chọn, đặt giá trị mặc định là No
        $featured = "No";
    }

    if (isset($_POST['active'])) {
        $active = $_POST['active'];
    } else {
        $active = "No";
    }

    if (isset($_FILES['image']['name'])) {
        // Tải lên hình ảnh
        // Để tải lên hình ảnh, chúng ta cần tên hình ảnh, đường dẫn nguồn và đường dẫn đích 
        $image_name = $_FILES['image']['name'];

        // Tải lên hình ảnh chỉ khi hình ảnh được chọn 
        if ($image_name != "") {
            //Tự động đổi tên hình ảnh
            // Lấy phần mở rộng của hình ảnh(jpg, png, gif, v.v.), ví dụ: "food1.jpg"
            $ext = end(explode('.', $image_name)); // .jpg

            //Đổi tên hình ảnh
            $image_name = "Food_Category_" . rand(000, 999) . '.' . $ext; // Vd: Food_Category_834.jpg

            $source_path = $_FILES['image']['tmp_name'];

            $destination_path = "../assets/img/category/" . $image_name;

            //tải lên hình ảnh
            $upload = move_uploaded_file($source_path, $destination_path); //Di chuyển tệp đã tải lên đến một vị trí mới

            // Kiểm tra xem hình ảnh có được tải lên hay không
            // Nếu hình ảnh không được tải lên thì dừng và chuyển hướng với thông báo lỗi 
            if ($upload == false) {
                //hiển thị thông báo và về trang add
                $_SESSION['upload'] = '<script>
                setTimeout(() => {
                    const notify = document.querySelector("#notify");
                    notify.onclick = () => {
                        showErrorToast("Tải hình ảnh thất bại !");
                    }
                    notify.click();
                }, 100);
                </script>';
                header('location:' . SITEURL . 'admin/category-add.php');
                die();
            }
        }
    } else {
        //Không tải lên hình ảnh và đặt giá trị image_name là rỗng
        $image_name = "";
    }

    //2. Tạo truy vấn SQL để chèn Category vào cơ sở dữ liệu 
    $sql = "INSERT INTO tbl_category SET title='$title',image_name='$image_name', featured='$featured',active='$active'";

    //3. Thực thi Truy vấn và Lưu trong Cơ sở dữ liệu 
    $res = mysqli_query($conn, $sql);

    //4. Kiểm tra xem truy vấn có được thực thi hay không và dữ liệu được thêm vào hay không 
    if ($res == true) {
        //hiển thị thông báo về trang manage
        $_SESSION['add'] = '<script>
        setTimeout(() => {
            const notify = document.querySelector("#notify");
            notify.onclick = () => {
                showSuccessToast("Thêm danh mục thành công !");
            }
            notify.click();
        }, 100);
        </script>';
        header('location:' . SITEURL . 'admin/category-manage.php');
    } else {
        //hiển thị thông báo về trang manage
        $_SESSION['add'] = '<script>
        setTimeout(() => {
            const notify = document.querySelector("#notify");
            notify.onclick = () => {
                showErrorToast("Thêm danh mục thất bại !");
            }
            notify.click();
        }, 100);
        </script>';
        header('location:' . SITEURL . 'admin/category-add.php');
    }
}
?>
        </div>
    </div>
</body>

</html>