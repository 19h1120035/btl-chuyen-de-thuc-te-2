    <nav class="nav">
        <ul>
            <li>
                <a href="index.php" class="sub-nav"><i class="fas fa-home"></i><span>Trang chủ</span></a>
            </li>
            <li>
                <a href="category-manage.php" class="sub-nav"><i class="fas fa-clipboard-list"></i><span>Danh mục</span></a>
            </li>
            <li>
                <a href="food-manage.php" class="sub-nav"><i class="fas fa-mortar-pestle"></i><span>Ẩm thực</span></a>
            </li>
            <li>
                <a href="order-manage.php" class="sub-nav"><i class="fas fa-shipping-fast"></i><span>Đơn hàng</span></a>
            </li>
            <li>
                <a href="admin-manage.php" class="sub-nav"><i class="fas fa-user-shield"></i><span>Quản lý admin</span></a>
            </li>
        </ul>

    </nav>