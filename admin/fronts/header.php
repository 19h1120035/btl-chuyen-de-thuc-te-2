<?php
include('../config/constants.php');
include('pagination.php');
ob_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quản trị viên</title>
    <link rel="stylesheet" href="./css/admin.css">
    <link rel="stylesheet" href="./css/notify.css">
    <link rel="stylesheet" href="../assets/fonts/fontawesome-free-5.15.4/css/all.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<?php
if (isset($_SESSION['user']) && isset($_SESSION['password'])) {
    $user = $_SESSION['user'];
    $pw = $_SESSION['password'];
}
$sql = "SELECT * FROM tbl_admin where username='$user' and password= '$pw'"; 
// truy vấn db
$res = mysqli_query($conn, $sql);
$row = mysqli_fetch_assoc($res);
//lấy id người dùng
$id = $row['id'];
?>

<body>
<div id="notify"></div>
    <header class="header">
        <a href="<?php echo SITEURL; ?>" class="logo">CucuFood</a>
        <div class="user">
            <p class="user-name"><?php echo $user; ?></p>
            <i class="fas fa-user user-img"></i>
            <div class="update">
                <ul>
                    <li class="user-item">
                        <a href="admin-update.php?id=<?php echo $id; ?>"><i class="fas fa-edit"></i> Sửa thông tin</a>
                    </li>
                    <li class="user-item">
                        <a href="update-password.php?id=<?php echo $id; ?>"><i class="fas fa-key"></i> Đổi mật khẩu</a>
                    </li>
                    <li class="user-item saparate">
                        <a href="logout.php"><i class="fas fa-sign-out-alt"></i> Đăng xuất</a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
