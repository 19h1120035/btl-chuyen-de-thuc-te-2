<?php
function pageResult($sql)
{
    $conn = mysqli_connect('localhost', 'root', '','order-food');
    mysqli_set_charset($conn, 'utf8');
    $data = [];
    $result = mysqli_query($conn, $sql);
    while ($row = mysqli_fetch_array($result)) {
        $data[] = $row;
    }
    mysqli_close($conn);
    return $data;
}