<?php
include('./fronts/header.php');
include('./fronts/navbar.php');
if (isset($_GET['page'])) {
    $curPage = $_GET['page'];
}else {
    $curPage =1;
}
?>
<script>
    const navs = document.querySelectorAll('.sub-nav');
    navs[1].style.color = 'red';
</script>
<div id="toast"></div>
<div class="manage">
    <div class="manage__head">
        <h2 class="title">Quản lý các danh mục sản phẩm</h2>
        <?php

if (isset($_SESSION['add'])) {
echo $_SESSION['add'];
unset($_SESSION['add']);
}

if (isset($_SESSION['remove'])) {
echo $_SESSION['remove'];
unset($_SESSION['remove']);
}

if (isset($_SESSION['delete'])) {
echo $_SESSION['delete'];
unset($_SESSION['delete']);
}

if (isset($_SESSION['no-category-found'])) {
echo $_SESSION['no-category-found'];
unset($_SESSION['no-category-found']);
}

if (isset($_SESSION['update'])) {
echo $_SESSION['update'];
unset($_SESSION['update']);
}

if (isset($_SESSION['upload'])) {
echo $_SESSION['upload'];
unset($_SESSION['upload']);
}

if (isset($_SESSION['failed-remove'])) {
echo $_SESSION['failed-remove'];
unset($_SESSION['failed-remove']);
}
?>
        <a href="<?php echo SITEURL; ?>admin/category-add.php" class="btn">
            <i class="fas fa-plus-circle"></i>
            <span>Thêm danh mục</span>
        </a>
    </div>
    <table class="manage__tbl">
        <tr>
            <th class="manage__tbl-th">#</th>
            <th class="manage__tbl-th">Tên danh mục</th>
            <th class="manage__tbl-th">Hình ảnh</th>
            <th class="manage__tbl-th">Đặc trưng</th>
            <th class="manage__tbl-th">Hoạt động</th>
            <th class="manage__tbl-th">Hành động</th>
        </tr>
        <?php
    /* =========Phân trang================ */
    $sql1 = "SELECT count(id) AS number FROM  tbl_category ";
    $result = pageResult($sql1);
    $number = 0;
    if ($result != null && count($result) > 0) {
        $number = $result[0]['number'];
    }
    $perPage = 6;
    $pages = ceil($number / $perPage);

    $current_page = 1;
    if (isset($_GET['page'])) {
        $current_page = $_GET['page'];
    }
    $index = ($current_page - 1) * $perPage;
    /* ========================================= */

    //Truy vấn để lấy tất cả các Danh mục từ Cơ sở dữ liệu
    $sql = "SELECT * FROM tbl_category LIMIT $index,$perPage";
    // Thực thi truy vấn
    $res = mysqli_query($conn, $sql);
    // Đếm hàng
    $count = mysqli_num_rows($res);
    //tạo biến seri_number và gán giá trị 
    $sn = $index + 1;
    //Kiểm tra xem dữ liệu có trong cơ sở dữ liệu hay không 
    if ($count > 0) {
        //Có dữ liệu
        // Lấy dữ liệu và hiển thị
        while ($row = mysqli_fetch_assoc($res)) {
            $id = $row['id'];
            $title = $row['title'];
            $image_name = $row['image_name'];
            $featured = $row['featured'];
            $active = $row['active'];
    ?>
        <tr>
            <td><?php echo $sn++; ?></td>
            <td class="max-width"><?php echo $title; ?></td>
            <td>
            <?php
                    //Kiểm tra xem tên hình ảnh có sẵn hay không 
                    if ($image_name != "") {
                        //Hiển thị hình ảnh
                    ?>
                        <img src="<?php echo SITEURL; ?>/assets/img/category/<?php echo $image_name; ?>" alt=""></td>
                    <?php
                    } else {
                        //Hiển thị thông báo
                        echo '<span style="color:red;">Hình ảnh không được thêm vào.</span>';
                    }
                    ?>
            <td>
                <?php 
                if ($featured == "Yes") {
                    echo "Có";
                }else {
                    echo "Không";
                }?>
            </td>
            <td> <?php 
                if ($active == "Yes") {
                    echo "Có";
                }else {
                    echo "Không";
                }?></td>
            <td>
                <a href="<?php echo SITEURL; ?>admin/category-update.php?id=<?php echo $id; ?>" class="btn-edit"><i class="fas fa-edit"></i></a>
                <a href="<?php echo SITEURL; ?>admin/category-delete.php?id=<?php echo $id; ?>&image_name=<?php echo $image_name; ?>" class="btn-delete"><i class="fas fa-trash-alt"></i></a>
            </td>
        </tr>
        <?php
        }
    } else {
        //không có dữ liệu
        //hiển thị thông báo
        ?>
        <tr>
            <td colspan="6">
                <div style="color:red;">Không có danh mục được thêm vào .</div>
            </td>
        </tr>
    <?php
    }
    ?>
    </table>
    <div class="pagination">
    <ul>
        <li>
            <div class="page-prev"> <i class="fas fa-angle-left"></i></div>
        </li>
        <!-- <li><a class="page page-checked" href="#">1</a></li>
        <li><a class="page" href="#">2</a></li>
        <li><a class="page" href="#">3</a></li>
        <li><a class="page" href="#">4</a></li>
        <li><a class="page" href="#">5</a></li>
        <li><a class="page" href="#">6</a></li> -->
        <?php
    for ($i = 1; $i <= $pages; $i++) { ?>
        <li><a class="page <?php if ($i == $curPage) { echo 'page-checked';}?>" href="?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
        <?php
    } ?>
        <li>
            <div class="page-next"> <i class="fas fa-angle-right"></i></div>
        </li>
    </ul>
</div>
</div>


<script src="./js/main.js"></script>
<script src="./js/notify.js"></script>
</body>

</html>