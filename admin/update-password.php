<?php
include('./fronts/header.php');
include('./fronts/navbar.php');
if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $error = array(); // Tạo arr để lưu thông báo

    // Validate cho pw hiện tại
    if (empty($_POST['current_password'])) {
        // Nếu mk rỗng thì lưu thông báo vào arr
        $error['current_password'] = "Vui lòng nhập mật khẩu hiện tại";
    } else {
        // Lấy id và pw vừa nhật
        $id_ad = $_POST['id'];
        $pw_current = md5($_POST['current_password']); // Mã hóa md5 cho pw vừa nhập để kiểm tra trong db
        $sql = "SELECT * FROM tbl_admin WHERE id=$id_ad AND password='$pw_current'";
        $res = mysqli_query($conn, $sql);
        if ($res == true) {
            $count = mysqli_num_rows($res);
            if ($count == 1) {
                // Nếu pw vừa nhập có trong database, thì gán $password_current = pw vừa nhập
                $password_current = $_POST['current_password'];
                
            } else {
                //  lưu thông báo lỗi
                $error['current_password'] = "Mật khẩu không hợp lệ!";
            }
        } else {
            $error['current_password'] = "Không tìm thấy người dùng!";
        }
    }

    // Validate cho new pw
    if (empty($_POST['new_password'])) {
        $error['new_password'] = "Vui lòng nhập mật khẩu mới";
    } else {
        if (strlen($_POST['new_password']) < 6) {
            // kiểm tra độ dài của pw nhập vào 
            $error['new_password'] = "Mật khẩu tối thiểu 6 kí tự";
        } else {
            $password_new = $_POST['new_password'];
        }
    }

    if (empty($_POST['confirm_password'])) {
        $error['confirm_password'] = "Vui lòng nhập lại mật khẩu";
    } else {
        $password_comfirm = $_POST['confirm_password'];
        if ($password_comfirm === $password_new) {
            $password_comfirm = $_POST['confirm_password'];
        } else {
            $error['confirm_password'] = "Mật khẩu nhập lại không khớp";
            $password_comfirm = "";
        }
    }
}

?>
    <script>
        const navs = document.querySelectorAll('.sub-nav');
        navs[4].style.color = 'red';
    </script>
    <div class="add">
        <div class="form-add">
            <h1 class="title text-center">Đổi Mật Khẩu</h1>
            <?php
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
        }
        ?>
            <form action="" method="POST" enctype="multipart/form-data">
                <div class="form-item">
                    <label class="form-label" for="current_password">Mật khẩu hiện tại: </label>
                    <input onInput="" class="form-input" type="password" name="current_password" id="current_password" placeholder="Nhập mật khẩu hiện tại" value="<?php if (isset($password_current)) { echo $password_current; } ?>">
                    <!-- <script>
                        function CheckPassword() {
                            var x = document.getElementById('current_password').value;
                            var str = "123456";
                            if (x != str) {
                                document.getElementById('current_password').style.borderColor = 'red';
                            }else{
                                document.getElementById('current_password').style.borderColor = 'green';
                            }
                        }
                    </script> -->
                    <?php
                if (isset($error['current_password'])) {
                ?>
                <span style="color: red;"><?php echo $error['current_password']; ?></span>
                <?php } ?>
                </div>
                <div class=" form-item ">
                    <label class="form-label " for="new_password">Mật khẩu mới: </label>
                    <input class="form-input" type="password" name="new_password" id="new_password" placeholder="Nhập mật khẩu mới" value="<?php if (isset($password_new)) {  echo $password_new; } ?>">
                    <?php
                if (isset($error['new_password'])) {
                ?>
                    <span style="color: red;"><?php echo $error['new_password']; ?></span>
                <?php } ?>
                </div>
                <div class=" form-item ">
                    <label class="form-label " for="confirm_password ">Nhập lại mật khẩu: </label>
                    <input class="form-input" type="password" name="confirm_password" id="confirm_password" placeholder="Nhập lại mật khẩu" value="<?php if (isset($password_comfirm)) { echo $password_comfirm; } ?>">
                    <?php
                if (isset($error['confirm_password'])) {
                ?>
                    <span style="color: red;"><?php echo $error['confirm_password']; ?></span>
                <?php } ?>
                </div>
                <input type="hidden" name="id" value="<?php echo $id; ?>">
                <input type="submit" name="submit" value="Đổi mật khẩu" class="form-btn ">
            </form>
        </div>
    </div>
    <?php

if (isset($password_current) && isset($password_new) && isset($password_comfirm)) {
    if (!empty($password_current && $password_new && $password_comfirm) && ($password_new === $password_comfirm)) {
        if (isset($_POST['submit'])) {
            $id = $_POST['id'];
            $new_password = md5($password_new);
            $sql2 = "UPDATE tbl_admin SET  password='$new_password' WHERE id=$id ";
            $res2 = mysqli_query($conn, $sql2);
            //Kiểm tra xem câu truy vấn có được thực thi hay không
            if ($res2 == true) {
                // Về trang manage admin và hiển thị thông báo
                $_SESSION['change-pwd'] =  '<script>
                setTimeout(() => {
                    const notify = document.querySelector("#notify");
                    notify.onclick = () => {
                        showSuccessToast("Đổi mật khẩu thành công !");
                    }
                         notify.click();
                }, 100);
                </script>';
                header('location:' . SITEURL . 'admin/admin-manage.php');
            } else {
                // Về trang chủ và hiển thị thông báo lỗi
                $_SESSION['change-pwd'] =  '<script>
                setTimeout(() => {
                    const notify = document.querySelector("#notify");
                    notify.onclick = () => {
                        showErrorToast("Đổi mật khẩu không thành công !");
                    }
                    notify.click();
                }, 100);
                </script>';
                header('location:' . SITEURL . 'admin/admin-manage.php');
            }
        }
    }
}
?>
</body>

</html>