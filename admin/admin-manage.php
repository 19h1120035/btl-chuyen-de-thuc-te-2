<?php
    include('./fronts/header.php');
    include('./fronts/navbar.php');
    if (isset($_GET['page'])) {
        $curPage = $_GET['page'];
    }else {
        $curPage =1;
    }
?>
<script>
    const navs = document.querySelectorAll('.sub-nav');
    navs[4].style.color = 'red';
</script>
    <div id="toast"></div>
    <div class="manage">
        <div class="manage__head">
            <h2 class="title">Quản trị viên</h2>
            <?php
                if (isset($_SESSION['add'])) {
                    echo $_SESSION['add']; //Hiển thị mục thông báo
                    unset($_SESSION['add']); //Xóa mục thông báo
                }
                if (isset($_SESSION['delete'])) {
                    echo $_SESSION['delete'];
                    unset($_SESSION['delete']);
                }
                if (isset($_SESSION['update'])) {
                    echo $_SESSION['update'];
                    unset($_SESSION['update']);
                }
                if (isset($_SESSION['change-pwd'])) {
                    echo $_SESSION['change-pwd'];
                    unset($_SESSION['change-pwd']);
                }
            ?>
            <a href="admin-add.php" class="btn">
                <i class="fas fa-plus-circle"></i>
                <span>Thêm admin</span>
            </a>
        </div>
        <table class="manage__tbl">
            <tr>
                <th class="manage__tbl-th">#</th>
                <th class="manage__tbl-th">Họ và tên</th>
                <th class="manage__tbl-th">Tên đăng nhập</th>
                <th class="manage__tbl-th">Mật khẩu</th>
                <th class="manage__tbl-th">Hành động</th>
            </tr>
            <?php
            /* =========Phân trang================ */
            $sql1 = "SELECT count(id) AS number FROM  tbl_admin ";
            $result = pageResult($sql1);
            $number = 0;
            if ($result != null && count($result) > 0) {
                $number = $result[0]['number'];
            }
            $perPage = 6;
            $pages = ceil($number / $perPage);
            $current_page = 1;
            if (isset($_GET['page'])) {
                $current_page = $_GET['page'];
            }
            $index = ($current_page - 1) * $perPage;
            /* ========================================= */
            //Truy vấn để nhận tất cả admin 
            $sql = "SELECT * FROM tbl_admin LIMIT $index,$perPage";
            //thực thi truy vấn
            $res = mysqli_query($conn, $sql);

            //Kiểm tra xem truy vấn có được thực thi không
            if ($res == TRUE) {
                // Đếm hàng để kiểm tra xem chúng ta có dữ liệu trong cơ sở dữ liệu hay không 
                $count = mysqli_num_rows($res); // Hàm lấy tất cả các hàng trong cơ sở dữ liệu 

                $sn = $index+1; //gán stt = 1

                //  Kiểm tra số lượng hàng 
                if ($count > 0) {
                //Có dữ liệu trong database
                    while ($rows = mysqli_fetch_assoc($res)) {
                        // Sử dụng vòng lặp While để lấy tất cả dữ liệu từ cơ sở dữ liệu.
                        $id = $rows['id'];
                        $full_name = $rows['full_name'];
                        $username = $rows['username'];
                        $password = $rows['password'];
                        // Hiển thị các giá trị trong bảng
            ?>
            <tr>
                <td><?php echo $sn++; ?></td>
                <td class="max-width"><?php echo $full_name; ?></td>
                <td><?php echo $username; ?></td>
                <td><?php echo $password; ?></td>
                <td>
                    <a href="<?php echo SITEURL; ?>admin/update-password.php?id=<?php echo $id; ?>" class="btn-key"><i class="fas fa-key"></i></a>
                    <a href="<?php echo SITEURL; ?>admin/admin-update.php?id=<?php echo $id; ?>" class="btn-edit"><i class="fas fa-edit"></i></a>
                    <a href="<?php echo SITEURL; ?>admin/admin-delete.php?id=<?php echo $id; ?>" class="btn-delete"><i class="fas fa-trash-alt"></i></a>
                </td>
            </tr>
            <?php
                }
            } else {
                //Không có dữ liệu trong database
            }
        }
        ?>
        </table>
        <div class="pagination">
        <ul>
            <li>
                <div class="page-prev"> <i class="fas fa-angle-left"></i></div>
            </li>
            <!-- <li><a class="page page-checked" href="#">1</a></li>
            <li><a class="page" href="#">2</a></li>
            <li><a class="page" href="#">3</a></li>
            <li><a class="page" href="#">4</a></li>
            <li><a class="page" href="#">5</a></li>
            <li><a class="page" href="#">6</a></li> -->
            <?php
        for ($i = 1; $i <= $pages; $i++) { ?>
            <li><a class="page <?php if ($i == $curPage) { echo 'page-checked';}?>" href="?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
            <?php
        } ?>
            <li>
                <div class="page-next"> <i class="fas fa-angle-right"></i></div>
            </li>
        </ul>
    </div>
    </div>

<script src="./js/main.js"></script>
<script src="./js/notify.js"></script>
</body>

</html>