<?php
include('./fronts/header.php');
include('./fronts/navbar.php');
// Kiểm tra xem người dùng đã đăng nhập chưa 
if (!isset($_SESSION['user'])) {
    $_SESSION['no-login-message'] = '<script>
    setTimeout(() => {
        const notify = document.querySelector("#notify");
        notify.onclick = () => {
            showWarningToast("Vui lòng đăng nhập để truy cập trang Admin !");
        }
        notify.click();
    }, 100);
    </script>';
    header('location:' . SITEURL . 'admin/login.php');
}

?>
<script>
    const navs = document.querySelectorAll('.sub-nav');
    navs[0].style.color = 'red';
</script>
<div id="toast"></div>
<div class="wrapper">
    <h2 class="title">Tổng quan</h2>
    <?php
    if (isset($_SESSION['login'])) {
        echo $_SESSION['login'];
        unset($_SESSION['login']);
    }
    if (isset($_SESSION['id'])) {
        echo $_SESSION['id'];
        unset($_SESSION['id']);
    }
    ?>
    <div class="overview">
        <a href="category-manage.php" class="overview-item">
            <div class="overview-head text-center">
                <?php

                $sql = "SELECT * FROM tbl_category";

                $res = mysqli_query($conn, $sql);

                $count = mysqli_num_rows($res);
                ?>
                <h1><?php echo $count; ?></h1>
                <span>Danh mục</span>
            </div>
        </a>
        <a href="food-manage.php" class="overview-item">
            <div class="overview-head text-center">
                <?php

                $sql2 = "SELECT * FROM tbl_food";

                $res2 = mysqli_query($conn, $sql2);

                $count2 = mysqli_num_rows($res2);
                ?>
                <h1><?php echo $count2; ?></h1>
                <span>Món ăn</span>
            </div>
        </a>
        <a href="order-manage.php" class="overview-item">
            <div class="overview-head text-center">
                <?php

                $sql3 = "SELECT * FROM tbl_order";

                $res3 = mysqli_query($conn, $sql3);

                $count3 = mysqli_num_rows($res3);
                ?>
                <h1><?php echo $count3; ?></h1>
                <span>Đơn hàng</span>
            </div>
        </a>
        <a href="#" class="overview-item">
            <div class="overview-head text-center">
                <?php
                // Tạo truy vấn SQL để nhận tổng doanh thu đã tạo 
                // Hàm tổng hợp trong SQL 
                $sql4 = "SELECT SUM(total) AS Total FROM tbl_order WHERE status='Delivered'";

                //Thực thi truy vấn
                $res4 = mysqli_query($conn, $sql4);

                //Nhận giá trị
                $row4 = mysqli_fetch_assoc($res4);

                //Nhận tổng doanh thu 
                $total_revenue = $row4['Total'];

                ?>
                <h1><?php echo number_format($total_revenue,3,'.','.')?>đ</h1>
                <span>Tổng doanh thu</span>
            </div>
        </a>
        <a href="order-manage.php" class="overview-item">
            <div class="overview-head text-center">
                <?php

                $sql6 = "SELECT * FROM tbl_order WHERE status = 'Ordered'";

                $res6 = mysqli_query($conn, $sql6);

                $count6 = mysqli_num_rows($res6);
                ?>
                <h1><?php echo $count6; ?></h1>
                <span>Đơn hàng đang chờ</span>
            </div>
        </a>
        <a href="order-manage.php" class="overview-item">
            <div class="overview-head text-center">
                <?php

                $sql7 = "SELECT * FROM tbl_order WHERE status = 'On Delivery'";

                $res7 = mysqli_query($conn, $sql7);

                $count7 = mysqli_num_rows($res7);
                ?>
                <h1><?php echo $count7; ?></h1>
                <span>Đang giao hàng</span>
            </div>
        </a>
        <a href="order-manage.php" class="overview-item">
            <div class="overview-head text-center">
                <?php

                $sql7 = "SELECT * FROM tbl_order WHERE status = 'Cancelled'";

                $res7 = mysqli_query($conn, $sql7);

                $count7 = mysqli_num_rows($res7);
                ?>
                <h1><?php echo $count7; ?></h1>
                <span>Đã hủy</span>
            </div>
        </a>
        <a href="admin-manage.php" class="overview-item">
            <div class="overview-head text-center">
                <?php

                $sql8 = "SELECT * FROM tbl_admin";

                $res8 = mysqli_query($conn, $sql8);

                $count8 = mysqli_num_rows($res8);
                ?>
                <h1><?php echo $count8; ?></h1>
                <span>Quản trị viên</span>
            </div>
        </a>
    </div>
</div>
<script src="./js/notify.js"></script>
</body>

</html>