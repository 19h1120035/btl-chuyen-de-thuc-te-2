-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th12 31, 2021 lúc 02:04 AM
-- Phiên bản máy phục vụ: 10.4.20-MariaDB
-- Phiên bản PHP: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `order-food`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(10) UNSIGNED NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `full_name`, `username`, `password`) VALUES
(1, 'Đào Văn Thương', 'admin', 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_category`
--

CREATE TABLE `tbl_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `featured` varchar(10) NOT NULL,
  `active` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `title`, `image_name`, `featured`, `active`) VALUES
(1, 'Bún / Phở', 'Food_Category_281.jpg', 'Yes', 'Yes'),
(2, 'Cơm', 'Food_Category_48.jpg', 'Yes', 'Yes'),
(4, 'Lẩu / Mì Cay', 'Food_Category_792.jpg', 'Yes', 'Yes'),
(5, 'Ăn Vặt', 'Food_Category_73.png', 'Yes', 'Yes'),
(6, 'Hải Sản', 'Food_Category_564.jpg', 'Yes', 'Yes'),
(7, 'Đồ Uống', 'Food_Category_733.png', 'Yes', 'Yes'),
(14, 'Healthy', 'Food_Category_65.png', 'Yes', 'Yes');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_food`
--

CREATE TABLE `tbl_food` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `price` decimal(10,3) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `featured` varchar(10) NOT NULL,
  `active` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tbl_food`
--

INSERT INTO `tbl_food` (`id`, `title`, `description`, `price`, `image_name`, `category_id`, `featured`, `active`) VALUES
(1, 'Bún Bò Huế', 'Beef Rice Noodles', '35.000', 'Food-Name-28.jpg', 1, 'Yes', 'Yes'),
(2, 'Bún Chả Cá Quy Nhơn', 'Fish ball noodles Quy Nhon', '50.000', 'Food-Name-1228.jpg', 1, 'Yes', 'Yes'),
(3, 'Mì Quảng', 'Mì Quảng đặc biệt (gà, tôm, trứng, cá, heo)', '45.000', 'Food-Name-3048.jpg', 1, 'Yes', 'Yes'),
(4, 'Cơm Gà Xối Mỡ', 'Cơm gà đùi lớn đặc biệt', '50.000', 'Food-Name-6162.jpg', 2, 'Yes', 'Yes'),
(6, 'Bún Riêu ', 'Bún riêu thập cẩm (giò, thịt, cua, chả)', '80.000', 'Food-Name-9821.jpg', 1, 'Yes', 'Yes'),
(8, 'Bánh Tráng Cuốn', 'Rice paper rolls', '20.000', 'Food-Name-1547.jpg', 5, 'Yes', 'Yes'),
(9, 'Bánh Tráng Nướng', 'Grilled rice paper', '20.000', 'Food-Name-8101.jpg', 5, 'Yes', 'Yes'),
(10, 'Bánh Tráng Trộn', 'Mixed rice paper', '20.000', 'Food-Name-8294.jpg', 5, 'Yes', 'Yes'),
(11, 'Bắp Xào Xúc Xích Xốt Phô Mai Trứng Muối', 'Stir fried corn with sausage and salted egg cheese sauce', '45.000', 'Food-Name-1929.jpg', 5, 'Yes', 'Yes'),
(12, 'Hủ Tiếu Xương', 'Noodle soup with pork bones', '45.000', 'Food-Name-8747.jpg', 1, 'Yes', 'Yes'),
(13, 'Hủ Tiếu Nam Vang', 'Special dried Nam Vang kuy teav - 1 can soft drink', '79.000', 'Food-Name-594.jpg', 1, 'Yes', 'Yes'),
(14, 'Bún Thịt Nướng', 'Grilled pork rice noodles, grilled fermented pork roll, fried spring roll', '45.000', 'Food-Name-9905.jpg', 1, 'Yes', 'Yes'),
(15, 'Khoai Tây Chiên', 'French fries', '20.000', 'Food-Name-8133.jpg', 5, 'Yes', 'Yes'),
(16, 'Chân Gà Xả Tắc', 'Lemongrass chicken feet', '60.000', 'Food-Name-4791.jpg', 5, 'Yes', 'Yes'),
(17, 'Trà Đào Cam Sả', 'Peach orange lemongrass tea', '45.000', 'Food-Name-3451.jpg', 7, 'Yes', 'Yes'),
(18, 'Trà Vải', 'Lychee tea', '45.000', 'Food-Name-2719.jpg', 7, 'Yes', 'Yes'),
(19, 'Cơm Sườn Cốt Lết', 'Rib cutlet rice - special part', '45.000', 'Food-Name-6037.jpg', 2, 'Yes', 'Yes'),
(21, 'Gà Rán KFC', 'KFC fried chicken', '89.000', 'Food-Name-2441.jpg', 5, 'Yes', 'Yes'),
(22, 'Trà Sữa Matcha', 'Matcha milk tea', '45.000', 'Food-Name-8857.jpg', 7, 'Yes', 'Yes'),
(23, 'Trà Sữa Truyền Thống', 'Traditional milk tea', '25.000', 'Food-Name-6381.jpg', 7, 'Yes', 'Yes'),
(24, 'Sữa Tươi Trân Châu Đường Đen', 'Black sugar pearl fresh milk', '39.000', 'Food-Name-5154.jpg', 7, 'Yes', 'Yes'),
(31, 'Rau Má Đậu Xanh Sữa Dừa', 'Rau má đậu xanh sữa dừa, thạch củ năng, trân châu ngọc trai, thạch lá dứa, thạch sợi', '35.000', 'Food-Name-6446.jpg', 7, 'Yes', 'Yes'),
(32, 'MiLô Dằm', 'Milo splinters', '40.000', 'Food-Name-465.jpg', 7, 'Yes', 'Yes'),
(33, 'Cà Phê Sữa Đá', 'Milk ice coffe', '25.000', 'Food-Name-3412.jpg', 7, 'Yes', 'Yes'),
(34, 'Tôm Hùm Alaska', 'Alaska lobster', '1400.000', 'Food-Name-4330.jpg', 6, 'Yes', 'Yes'),
(35, 'Ốc Xào Xả Ớt', 'Stir-fried snails with lemongrass and chili', '65.000', 'Food-Name-597.jpg', 5, 'Yes', 'Yes'),
(36, 'Cơm Gà Chiên Nước Mắm', 'Fried chicken rice with fish sauce', '45.000', 'Food-Name-6573.jpg', 2, 'Yes', 'Yes'),
(37, 'Cơm Sườn Bì Chả Đặc Biệt', 'Cơm sườn + bì + chả + nước tùy chọn + khăn lạnh', '69.000', 'Food-Name-5819.jpg', 2, 'Yes', 'Yes'),
(38, 'Lẩu Thái Hải Sản', 'Lẩu thái chua cay hải sản', '219.000', 'Food-Name-4877.jpg', 4, 'Yes', 'Yes'),
(39, 'Lẩu Tomyum Hải Sản', 'Lẩu tomyum hải sản đặc biệt', '189.000', 'Food-Name-2952.jpg', 4, 'Yes', 'Yes'),
(40, 'Mì Cay Hải Sản', 'Spicy seafood noodles', '49.000', 'Food-Name-7677.jpg', 4, 'Yes', 'Yes'),
(41, 'Cơm Cháy Chà Bông', 'Scrambled rice with cotton balls', '45.000', 'Food-Name-5227.jpg', 2, 'Yes', 'Yes'),
(42, 'Cơm Ba Rọi', 'Grilled pork belly rice', '45.000', 'Food-Name-3646.jpg', 2, 'Yes', 'Yes'),
(43, 'Cơm Chiên Dương Châu', 'Yangzhou fried rice', '45.000', 'Food-Name-3342.jpg', 2, 'Yes', 'Yes'),
(44, 'Cơm Chiên Hải Sản', 'Seafood fried rice', '45.000', 'Food-Name-3922.jpg', 2, 'Yes', 'Yes'),
(45, 'Cơm Sườn Xào Chua Ngọt', 'Stir-fried sweet and sour ribs rice', '45.000', 'Food-Name-9553.jpg', 2, 'Yes', 'Yes'),
(46, 'Hủ Tiếu Bò Kho', 'Beef noodles', '45.000', 'Food-Name-2934.jpg', 1, 'Yes', 'Yes'),
(47, 'Hủ Tiếu Giò Heo', 'Noodle soup with pig feet', '45.000', 'Food-Name-3597.jpg', 1, 'Yes', 'Yes'),
(48, 'Mực Nướng Muối Ớt', 'Grilled squid with salt and pepper', '249.000', 'Food-Name-7168.jpg', 6, 'Yes', 'Yes'),
(49, 'Mực Khô', 'Dried octopus', '139.000', 'Food-Name-7169.jpg', 5, 'Yes', 'Yes'),
(50, 'Hồng Trà', 'Red tea', '40.000', 'Food-Name-2090.jpg', 7, 'Yes', 'Yes');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_order`
--

CREATE TABLE `tbl_order` (
  `id` int(10) UNSIGNED NOT NULL,
  `food` varchar(150) NOT NULL,
  `price` decimal(10,3) NOT NULL,
  `qty` int(11) NOT NULL,
  `total` decimal(10,3) NOT NULL,
  `order_date` datetime NOT NULL,
  `status` varchar(50) NOT NULL,
  `customer_name` varchar(150) NOT NULL,
  `customer_contact` varchar(20) NOT NULL,
  `customer_email` varchar(150) NOT NULL,
  `customer_address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tbl_order`
--

INSERT INTO `tbl_order` (`id`, `food`, `price`, `qty`, `total`, `order_date`, `status`, `customer_name`, `customer_contact`, `customer_email`, `customer_address`) VALUES
(1, 'Bún Bò Huế', '45.000', 2, '90.000', '2021-12-24 01:28:22', 'Delivered', 'Đào Văn Thương', '0333729170', 'chuthuong1080@gmail.com', 'Biên Hòa'),
(22, 'Cơm Gà Xối Mỡ', '50.000', 10, '500.000', '2021-12-25 17:04:44', 'Delivered', 'Đào Văn Thương', '0333729170', 'chuthuong1080@gmail.com', 'Biên Hòa\r\n'),
(23, 'Bún Bò Huế', '45.000', 100, '4500.000', '2021-12-26 09:55:17', 'Delivered', 'Chu Văn Thương', '0921775630', 'daon8908@gmail.com', 'Biên Hòa'),
(24, 'Bún Bò Huế', '45.000', 1, '45.000', '2021-12-27 10:23:58', 'Ordered', 'Đào Văn Thương', '0395493768', 'chuthuong1080@gmail.com', 'Biên Hòa'),
(32, 'Bún thịt nướng', '40.000', 1, '40.000', '2021-12-28 07:50:02', 'Ordered', 'Đào Văn Thương', '0982696803', 'chuthuong1080@gmail.com', 'Biên Hòa'),
(37, 'Bún thịt nướng', '40.000', 1, '40.000', '2021-12-28 08:23:53', 'Ordered', 'Chu Văn Thương', '0937443884', '19h1120035@sv.ut.edu.vn', 'Biên Hòa\r\n'),
(41, 'Cơm Gà Xối Mỡ', '50.000', 1, '50.000', '2021-12-28 14:53:45', 'Cancelled', 'Đào Văn Thương', '0333729170', 'chuthuong1080@gmail.com', 'Biên Hòa'),
(42, 'Mì Quảng', '45.000', 1, '45.000', '2021-12-28 15:51:34', 'Ordered', 'Đào Văn Thương', '0333729170', 'chuthuong1080@gmail.com', 'Biên Hòa'),
(43, 'Bún riêu', '50.000', 1, '50.000', '2021-12-28 16:04:46', 'Cancelled', 'Chu Thương', '0333729170', 'hieucute2511@gmail.com', 'Biên Hòa'),
(44, 'Bánh tráng trộn', '20.000', 1, '20.000', '2021-12-28 16:06:15', 'Delivered', 'Đào Văn Thương', '0333729170', '19h1120035@sv.ut.edu.vn', 'Biên Hòa'),
(45, 'Bún riêu', '50.000', 1, '50.000', '2021-12-28 16:32:47', 'Ordered', 'ghngfj', '4644544456', 'hieucute2511@gmail.com', 'ghthjtyhty'),
(46, 'Cơm gà xới mỡ', '35.000', 1, '35.000', '2021-12-28 16:33:21', 'Ordered', 'sgfsdg', '4563564567', '19h1120035@sv.ut.edu.vn', 'sfgdsfg'),
(47, 'Bún Chả Cá Quy Nhơn', '50.000', 2000, '100000.000', '2021-12-28 16:50:58', 'On Delivery', 'Đào Văn Thương', '0333729170', 'chuthuong1080@gmail.com', 'Biên Hòa'),
(48, 'Hủ tiếu xương', '45.000', 1, '45.000', '2021-12-29 11:44:20', 'Delivered', 'Đào Văn Thương', '0333729170', 'chuthuong1080@gmail.com', 'Biên Hòa'),
(49, 'Bánh tráng trộn', '20.000', 1, '20.000', '2021-12-29 17:26:10', 'Ordered', 'Đào Văn Thương', '0867749370', 'daon8908@gmail.com', 'Biên Hòa'),
(51, 'Bắp xào', '20.000', 1, '20.000', '2021-12-30 07:21:52', 'Ordered', 'Đào Văn Thương', '0931234835', '19h1120092@sv.ut.edu.vn', 'Biên Hòa'),
(52, 'Tôm Hùm Alaska', '1400.000', 10, '14000.000', '2021-12-30 08:47:40', 'On Delivery', 'Ngô Thanh Hà', '0823794287', 'hathanh.ngo@gmail.com', 'Quy Nhơn'),
(53, 'Mực Nướng Muối Ớt', '249.000', 1, '249.000', '2021-12-30 16:39:44', 'Ordered', 'Đào Văn Thương', '0333729170', 'chuthuong1080@gmail.com', 'sdcsdc');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_food`
--
ALTER TABLE `tbl_food`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT cho bảng `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT cho bảng `tbl_food`
--
ALTER TABLE `tbl_food`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT cho bảng `tbl_order`
--
ALTER TABLE `tbl_order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
