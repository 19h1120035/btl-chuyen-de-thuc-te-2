<?php
include('./partials-front/header.php');
include('./partials-front/banner.php');
if (isset($_GET['page'])) {
    $curPage = $_GET['page'];
}else {
    $curPage =1;
}
?>
<script>
    const navs = document.querySelectorAll('.nav-link');
    navs[2].style.color = '#008080';
    const mobileNavs = document.querySelectorAll('.nav-mobile-link');
    mobileNavs[2].style.color = 'red';
</script>
<div class="wrapper">
    <div class="foods">
        <h1 class="title">Thực đơn</h1>
        <div class="food">
            <?php
            /* =========Phân trang================ */
            $sql1 = "SELECT count(id) AS number FROM tbl_food";
            $result = pageResult($sql1);
            $number = 0;
            if ($result != null && count($result) > 0) {
                $number = $result[0]['number'];
            }
            $perPage = 8;
            $pages = ceil($number / $perPage);
            $current_page = 1;
            if (isset($_GET['page'])) {
                $current_page = $_GET['page'];
            }
            $index = ($current_page - 1) * $perPage;
            /* ========================================= */
            //Hiển thị thực phẩm đang hoạt động
            $sql = "SELECT * FROM tbl_food WHERE active='Yes' LIMIT $index,$perPage";
            // $sql = "SELECT * FROM tbl_food WHERE active='Yes'";

            //Thực thi truy vấn
            $res = mysqli_query($conn, $sql);

            //Đếm hàng
            $count = mysqli_num_rows($res);

            //Kiểm tra xem thực phẩm còn dùng được hay không 
            if ($count > 0) {
                //Ok
                while ($row = mysqli_fetch_assoc($res)) {
                    //Nhận all giá trị
                    $id = $row['id'];
                    $title = $row['title'];
                    $description = $row['description'];
                    $price = $row['price'];
                    $image_name = $row['image_name'];
            ?>
                    <div class="food-item">
                        <div class="food-img">
                            <?php
                            //Kiểm tra xem hình ảnh có sẵn hay không 
                            if ($image_name == "") {
                                //Not OK
                                echo "<div class='error'>Hình ảnh không có sẵn.</div>";
                            } else {
                                //OK
                            ?>
                                <img src="<?php echo SITEURL; ?>assets/img/food/<?php echo $image_name; ?>">
                            <?php } ?>
                        </div>
                        <div class="food-info">
                            <h3 class="food-title"><?php echo $title; ?></h3>
                            <span class="food-price"><?php echo number_format($price,3 ,'.','.'); ?>đ</span>
                            <p class="food-desc"><?php echo $description; ?></p>
                            <a href="<?php echo SITEURL; ?>order.php?food_id=<?php echo $id; ?>" class="btn-order">Đặt hàng ngay</a>
                        </div>
                    </div>
            <?php
                }
            } else {
                //Thức ăn không có sẵn 
                echo "<div class='error'>Không tìm thấy món ăn.</div>";
            }
            ?>
        </div>
    </div>
</div>
<div class="pagination">
    <ul>
        <li>
            <div class="page-prev"> <i class="fas fa-angle-left"></i></div>
        </li>
        <?php
        for ($i = 1; $i <= $pages; $i++) { ?>
            <li><a class="page <?php if ($i == $curPage) { echo 'page-checked';}?>" href="?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
            <?php
        } ?>
        <li>
            <div class="page-next"> <i class="fas fa-angle-right"></i></div>
        </li>
    </ul>
</div>
<?php
include('./partials-front/footer.php');
?>
<a href="# " class="back-to-top">
    <i class="fas fa-angle-up"></i>
</a>
<script src="./assets/js/slider.js"></script>
<script src="./assets/js/main.js"></script>
<script src="./assets/js/pagination.js"></script>
</body>

</html>