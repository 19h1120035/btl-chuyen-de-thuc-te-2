<footer class="footer" id="contact">
        <div class="link">
            <div class="support">
                <h3 class="contact-title">Tổng đài hỗ trợ</h3>
                <p>Hotline: <a href="tel:0333729170" class="sub-link phone">0333729170</a></p>
                <p>Địa chỉ: 30 đường Nguyễn Ái Quốc, phường Tân Phong, Thành phố Biên Hòa, tỉnh Đồng Nai.</p>
                <p>Email: <a href="mailto:chuthuong1080@gmail.com" class="sub-link email">chuthuong1080@gmail.com</a></p>
            </div>
            <div class="introduce">
                <h3 class="contact-title">Giới thiệu</h3>
                <ul>
                    <li><a href="#" class="sub-link">Hệ thống nhà hàng</a></li>
                    <li><a href="#" class="sub-link">Câu chuyện thương hiệu</a></li>
                    <li><a href="#" class="sub-link">Ưu đãi thành viên</a></li>
                    <li><a href="#" class="sub-link">Tin tức và sự kiện</a></li>
                    <li><a href="#" class="sub-link">Tuyển đãi</a></li>
                </ul>
            </div>
            <div class="contact">
                <h3 class="contact-title">Liên hệ</h3>
                <ul>
                    <li><a href="#" class="sub-link">Liên hệ</a></li>
                    <li><a href="#" class="sub-link">Hướng dẫn đặt hàng</a></li>
                    <li><a href="#" class="sub-link">Chính sách giao hàng</a></li>
                    <li><a href="#" class="sub-link">Chính sách bảo mật</a></li>
                    <li><a href="#" class="sub-link">Điều khoản và điều kiện</a></li>
                </ul>
            </div>
            <div class="sign-up">
                <h3 class="contact-title">Đăng kí nhận tin</h3>
                <p>Nhận thông tin sản phẩm mới nhất, tin khuyến mãi và nhiều hơn nữa.</p>
                <input type="email" name="email" id="email" placeholder="Email của bạn" required>
                <input type="submit" class="btn-sign-up" value="Đăng kí nhận tin"></input>
            </div>
        </div>
        <div class="social">
            <div class="social-network">
                <ul>
                    <li>
                        <a class="facebook" href="https://www.facebook.com/thuongchu20"><img src="https://img.icons8.com/fluent/50/000000/facebook-new.png" /></a>
                    </li>
                    <li>
                        <a class="instagram" href="https://www.instagram.com/abs.204t/"><img src="https://img.icons8.com/fluent/48/000000/instagram-new.png" /></a>
                    </li>
                    <li>
                        <a class="youtube" href="https://www.youtube.com/channel/UCoFsNzkW56j91da70FQq52w"><img src="https://img.icons8.com/color/48/000000/youtube-play.png" /></a>
                    </li>
                </ul>
            </div>
            <div class="copyright">
                <p>Copyright not registered. Designed by
                    <a class="logo" href="<?php echo SITEURL; ?>"> CucuFood </a>Corporation.</p>
            </div>
        </div>
    </footer>