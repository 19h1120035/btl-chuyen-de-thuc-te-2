
    <!-- image slider start -->
    <div class="slider">
        <div class="slides">
            <form action="<?php echo SITEURL; ?>food-search.php" method="GET">
                <input type="search" name="search" placeholder="Tìm kiếm món ăn" required>
                <input type="submit" name="submit" value="Tìm kiếm" id="" class="btn-search">
            </form>
            <i class="fas fa-angle-left slide-prev"></i>
            <!-- radio buttons start -->
            <input type="radio" name="radio-btn" id="radio1">
            <input type="radio" name="radio-btn" id="radio2">
            <input type="radio" name="radio-btn" id="radio3">
            <input type="radio" name="radio-btn" id="radio4">
            <!-- radio buttons end -->
            <!-- slide images start -->
            <div class="slide first">
                <img src="./assets/img/slider/tra_sua_matcha.jpg" alt="" id="img-slide">
            </div>
            <div class="slide">
                <img src="./assets/img/slider/bunbo.jpg" alt="" id="img-slide">
            </div>
            <div class="slide">
                <img src="./assets/img/slider/com_ba_roi.jpg" alt="" id="img-slide">
            </div>
            <div class="slide">
                <img src="./assets/img/slider/mi_quang.jpg" alt="" id="img-slide">
            </div>
            <!-- slide images end -->
            <!-- automatic navigation start -->
            <div class="navigation-auto">
                <div class="auto-btn1"></div>
                <div class="auto-btn2"></div>
                <div class="auto-btn3"></div>
                <div class="auto-btn4"></div>
            </div>
            <!-- automatic navigation end -->
            <i class="fas fa-angle-right slide-next"></i>
        </div>
        <!-- manual navigation start -->
        <div class="navigation-manual">
            <label for="radio1" class="manual-btn"></label>
            <label for="radio2" class="manual-btn"></label>
            <label for="radio3" class="manual-btn"></label>
            <label for="radio4" class="manual-btn"></label>
        </div>
        <!-- manual navigation end -->

    </div>
    <!-- image slider end -->
