<?php
include('config/constants.php');
require_once('partials-front/pagination.php');
ob_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hệ Thống Đặt Đồ Ăn Trực Tuyến</title>

    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <link rel="stylesheet" href="assets/css/notify.css">
    <link rel="stylesheet" href="assets/fonts/fontawesome-free-5.15.4/css/all.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>

<body>
    <div id="notify"></div>
    <header class="header">
        <a class="logo" href="<?php echo SITEURL; ?>">CucuFood</a>
        <ul class="nav">
            <li class="nav-list"><a class="nav-link" href="<?php echo SITEURL; ?>">Trang chủ</a></li>
            <li class="nav-list"><a class="nav-link" href="<?php echo SITEURL; ?>categories.php">Danh mục</a></li>
            <li class="nav-list"><a class="nav-link" href="<?php echo SITEURL; ?>foods.php">Ẩm thực</a></li>
            <li class="nav-list"><a class="nav-link" href="#contact">Liên hệ</a></li>
        </ul>
        <ul class="nav">
            <li class="nav-list"><a class="nav-link" href="<?php echo SITEURL; ?>admin/login.php">Đăng nhập</a></li>
        </ul>
        <ul class="nav-mobile">
            <li class="nav-mobile-list">
                <a class="nav-mobile-link" href="<?php echo SITEURL; ?>">
                    <div class="icon">
                        <i class="fas fa-home"></i>
                    </div>
                    <span>Trang chủ</span>
                </a>
            </li>
            <li class="nav-mobile-list">
                <a class="nav-mobile-link" href="<?php echo SITEURL; ?>categories.php">
                    <div class="icon">
                        <i class="fas fa-clipboard-list"></i>
                    </div>
                    <span>Danh mục</span>
                </a>
            </li>
            <li class="nav-mobile-list">
                <a class="nav-mobile-link" href="<?php echo SITEURL; ?>foods.php">
                    <div class="icon">
                        <i class="fas fa-mortar-pestle"></i>
                    </div>
                    <span>Ẩm thực</span>
                </a>
            </li>
            <li class="nav-mobile-list">
                <a class="nav-mobile-link" href="#contact">
                    <div class="icon">
                        <i class="fas fa-phone"></i>
                    </div>
                    <span>Liên hệ</span>
                </a>
            </li>
            <li class="nav-mobile-list">
                <a class="nav-mobile-link" href="<?php echo SITEURL; ?>admin/login.php">
                    <div class="icon">
                        <i class="fas fa-sign-in-alt"></i>
                    </div>
                    <span>Đăng nhập</span>
                </a>
            </li>
        </ul>
        <div class="menu-toggle"><i class="fa fa-bars" aria-hidden="true"></i></div>
    </header>
