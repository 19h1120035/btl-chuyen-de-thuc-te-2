<?php
include('./partials-front/header.php');
if (isset($_GET['page'])) {
    $curPage = $_GET['page'];
}else {
    $curPage =1;
}
?>
<script>
    const navs = document.querySelectorAll('.nav-link');
    navs[2].style.color = '#008080';
    const mobileNavs = document.querySelectorAll('.nav-mobile-link');
    mobileNavs[2].style.color = 'red';
</script>
<!-- image slider start -->
<div class="slider">
    <div class="slides">
        <?php
        if (isset($_GET['search'])) {
            //Lấy từ khóa tìm kiếm
            $search = $_GET['search'];
        }
        else{
            $search = 'bún';
        }

        ?>
        <h2 class="food-search">Kết quả tìm kiếm món ăn của bạn"<?php echo $search; ?>"</h2>
        <i class="fas fa-angle-left slide-prev"></i>
        <!-- radio buttons start -->
        <input type="radio" name="radio-btn" id="radio1">
        <input type="radio" name="radio-btn" id="radio2">
        <input type="radio" name="radio-btn" id="radio3">
        <input type="radio" name="radio-btn" id="radio4">
        <!-- radio buttons end -->
        <!-- slide images start -->
        <div class="slide first">
            <img src="./assets/img/slider/tra_sua_matcha.jpg" alt="" id="img-slide">
        </div>
        <div class="slide">
            <img src="./assets/img/slider/bunbo.jpg" alt="" id="img-slide">
        </div>
        <div class="slide">
            <img src="./assets/img/slider/com_ba_roi.jpg" alt="" id="img-slide">
        </div>
        <div class="slide">
            <img src="./assets/img/slider/mi_quang.jpg" alt="" id="img-slide">
        </div>
        <!-- slide images end -->
        <!-- automatic navigation start -->
        <div class="navigation-auto">
            <div class="auto-btn1"></div>
            <div class="auto-btn2"></div>
            <div class="auto-btn3"></div>
            <div class="auto-btn4"></div>
        </div>
        <!-- automatic navigation end -->
        <i class="fas fa-angle-right slide-next"></i>
    </div>
    <!-- manual navigation start -->
    <div class="navigation-manual">
        <label for="radio1" class="manual-btn"></label>
        <label for="radio2" class="manual-btn"></label>
        <label for="radio3" class="manual-btn"></label>
        <label for="radio4" class="manual-btn"></label>
    </div>
    <!-- manual navigation end -->

</div>
<!-- image slider end -->
<div class="wrapper">
    <div class="foods">
        <h1 class="title">Thực đơn</h1>
        <div class="food">
            <?php
            /* =========Phân trang================ */
            $sql1 = "SELECT count(id) AS number FROM tbl_food WHERE title LIKE '%$search%' OR description LIKE '%$search%'";
            $result = pageResult($sql1);
            $number = 0;
            if ($result != null && count($result) > 0) {
                $number = $result[0]['number'];
            }
            $perPage = 8;
            $pages = ceil($number / $perPage);
            $current_page = 1;
            if (isset($_GET['page'])) {
                $current_page = $_GET['page'];
            }
            $index = ($current_page - 1) * $perPage;
            /* ========================================= */
            //Truy vấn SQL để lấy thực phẩm dựa trên từ khóa tìm kiếm 
            $sql = "SELECT * FROM tbl_food WHERE title LIKE '%$search%' OR description LIKE '%$search%' LIMIT $index,$perPage";
            $res = mysqli_query($conn, $sql);
            //Đếm hàng
            $count = mysqli_num_rows($res);
            //Kiểm tra xem thức ăn có sẵn của không 
            if ($count > 0) {
                //OK
                while ($row = mysqli_fetch_assoc($res)) {
                    //lấy all giá trị
                    $id = $row['id'];
                    $title = $row['title'];
                    $price = $row['price'];
                    $description = $row['description'];
                    $image_name = $row['image_name'];
            ?>
                    <div class="food-item">
                        <div class="food-img">
                            <?php
                            // Kiểm tra xem tên hình ảnh có sẵn hay không 
                            if ($image_name == "") {
                                //Not OK
                                echo "<div class='error'>Hình ảnh không có sẵn .</div>";
                            } else {
                            ?>
                                <img src="<?php echo SITEURL; ?>assets/img/food/<?php echo $image_name; ?>">
                            <?php } ?>
                        </div>
                        <div class="food-info">
                            <h3 class="food-title"><?php echo $title; ?></h3>
                            <span class="food-price"><?php echo number_format($price,3 ,'.','.'); ?>đ</span>
                            <p class="food-desc"><?php echo $description; ?></p>
                            <a href="<?php echo SITEURL; ?>/order.php?food_id=<?php echo $id; ?>" class="btn-order">Đặt hàng ngay</a>
                        </div>
                    </div>
            <?php
                }
            } else {
                //Thức ăn không có sẵn 
                echo "<div class='error'>Món ăn không có sẵn.</div>";
            }
            ?>
        </div>
    </div>
</div>
<div class="pagination">
    <ul>
        <li>
            <div class="page-prev"> <i class="fas fa-angle-left"></i></div>
        </li>
        <?php
        for ($i = 1; $i <= $pages; $i++) { ?>
            <li><a class="page <?php if ($i == $curPage) { echo 'page-checked';}?>" href="?search=<?php echo $search;?>&page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
            <?php
        } ?>
        <li>
            <div class="page-next"> <i class="fas fa-angle-right"></i></div>
        </li>
    </ul>
</div>
<?php
include('./partials-front/footer.php');
?>
<a href="#" class="back-to-top">
    <i class="fas fa-angle-up"></i>
</a>
<script src="./assets/js/slider.js"></script>
<script src="./assets/js/main.js"></script>
<script src="./assets/js/pagination.js"></script>
</body>

</html>