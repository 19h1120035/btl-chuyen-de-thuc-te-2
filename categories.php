<?php
include ('./partials-front/header.php');
if (isset($_GET['page'])) {
    $curPage = $_GET['page'];
}else {
    $curPage =1;
}
?>
<script>
    const navs = document.querySelectorAll('.nav-link');
    navs[1].style.color = '#008080';
    const mobileNavs = document.querySelectorAll('.nav-mobile-link');
    mobileNavs[1].style.color = 'red';
</script>
<div class="container">
    <div class="categorys">
        <h1 class="title">Khám phá các danh mục ẩm thực</h1>
        <div class="category">
        <?php
    /* =========Phân trang================ */
    $sql1 ="SELECT count(id) AS number FROM tbl_category";
    $result = pageResult($sql1);
    $number = 0;
    if ($result != null && count($result) > 0) {
        $number = $result[0]['number'];
    }
    $perPage = 6;
    $pages = ceil($number / $perPage);
    $current_page = 1;
    if (isset($_GET['page'])) {
        $current_page = $_GET['page'];
    }
    $index = ($current_page - 1) * $perPage;
    /* ========================================= */
    // Hiển thị tất cả các danh mục đang hoạt động 
    // Truy vấn Sql 
    $sql ="SELECT * FROM tbl_category WHERE active='Yes' LIMIT $index,$perPage";
    //Thực thi truy vấn
    $res = mysqli_query($conn, $sql);
    // Đếm hàng
    $count = mysqli_num_rows($res);
    //Kiểm tra xem các danh mục có sẵn hay không
    if ($count > 0) {
        while ($row = mysqli_fetch_assoc($res)) {
            //Lấy all giá trị 
            $id = $row['id'];
            $title = $row['title'];
            $image_name = $row['image_name'];
    ?>
            <a href="<?php echo SITEURL; ?>category-foods.php?category_id=<?php echo $id; ?>" class="category-item">
                <span><?php echo $title; ?></span>
                <?php
                    if ($image_name =="") {
                        //Không tìm thấy hình ảnh 
                        echo"<div class='error'>Không tìm thấy hình ảnh .</div>";
                    } else {
                        //Ok
                    ?>
                        <img src="<?php echo SITEURL; ?>assets/img/category/<?php echo $image_name; ?>">
                    <?php } ?>
            </a>
            <?php
        }
    } else {
        //Danh mục không được tìm thấy
        echo"<div class='error'>Danh mục không được tìm thấy.</div>";
    }
    ?>
        </div>
    </div>
</div>
<div class="pagination">
    <ul>
        <li>
            <div class="page-prev"> <i class="fas fa-angle-left"></i></div>
        </li>
        <?php
        for ($i = 1; $i <= $pages; $i++) { ?>
            <li><a class="page <?php if ($i == $curPage) { echo 'page-checked';}?>" href="?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
            <?php
        } ?>
        <li>
            <div class="page-next"> <i class="fas fa-angle-right"></i></div>
        </li>
    </ul>
</div>
<?php
include('./partials-front/footer.php');
?>
   <a href="#" class="back-to-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <script src="./assets/js/main.js"></script>
    <script src="./assets/js/pagination.js"></script>
</body>
</html>