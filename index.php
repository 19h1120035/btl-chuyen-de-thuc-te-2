<?php
include('./partials-front/header.php');
include('./partials-front/banner.php');
?>
<script>
    const navs = document.querySelectorAll('.nav-link');
    navs[0].style.color = '#008080';
    const mobileNavs = document.querySelectorAll('.nav-mobile-link');
    mobileNavs[0].style.color = 'red';
</script>

<?php
if (isset($_SESSION['order'])) {
    echo $_SESSION['order'];
    unset($_SESSION['order']);
}
?>
<div id="toast"></div>
<div class="container">
    <div class="categorys">
        <h1 class="title">Khám Phá Các Danh Mục Ẩm Thực Khác Nhau</h1>

        <div class="category">
            <?php
            //Tạo truy vấn SQL để hiển thị các danh mục từ cơ sở dữ liệu 
            $sql = "SELECT * FROM tbl_category WHERE active='Yes' AND featured='Yes' ORDER BY id DESC LIMIT 6";
            //order by -  desc : sắp xếp thứ tự của res giảm dần
            //Thực thi truy vấn
            $res = mysqli_query($conn, $sql);
            //Đếm hàng để kiểm tra xem danh mục có sẵn hay không 
            $count = mysqli_num_rows($res);
            if ($count > 0) {
                //Các danh mục có sẵn 
                while ($row = mysqli_fetch_assoc($res)) {
                    //Nhận các giá trị: id, title, image_name 
                    $id = $row['id'];
                    $title = $row['title'];
                    $image_name = $row['image_name'];
            ?>
                    <a href="<?php echo SITEURL; ?>category-foods.php?category_id=<?php echo $id; ?>" class="category-item">

                        <span><?php echo $title; ?></span>
                        <?php
                        //Kiểm tra xem hình ảnh có sẵn hay không 
                        if ($image_name == "") {
                            //Hiển thị thông báo 
                            echo "<div class='error'>Hình ảnh không có sẵn</div>";
                        } else {
                            //Hình ảnh có sẵn
                        ?>
                            <img src="<?php echo SITEURL; ?>/assets/img//category/<?php echo $image_name; ?>">
                        <?php
                        }
                        ?>
                    </a>
            <?php
                }
            } else {
                //Danh mục không có sẵn 
                echo '<script>alert("Danh mục không được thêm vào !");</script>';
            }
            ?>
        </div>
        <a href="<?php echo SITEURL; ?>categories.php" class="btn-all">Xem tất cả</a>
    </div>
</div>
<div class="wrapper">
    <div class="foods">
        <h1 class="title">Thực đơn</h1>
        <div class="food">
        <?php

//Lấy food từ Cơ sở dữ liệu đang hoạt động và nổi bật 
//SQL Query
$sql2 = "SELECT * FROM tbl_food WHERE active='Yes' AND featured='Yes' LIMIT 10";
//Thực thi truy vấn
$res2 = mysqli_query($conn, $sql2);
//Đếm hàng
$count2 = mysqli_num_rows($res2);
// Kiểm tra xem thức ăn có sẵn hay không 
if ($count2 > 0) {
    //Thức ăn có sẵn
    while ($row = mysqli_fetch_assoc($res2)) {
        //Nhận all giá trị
        $id = $row['id'];
        $title = $row['title'];
        $price = $row['price'];
        $description = $row['description'];
        $image_name = $row['image_name'];
?>
            <div class="food-item">
                <div class="food-img">
                <?php
                        //Kiểm tra xem hình ảnh có sẵn hay không 
                        if ($image_name == "") {
                            //Hỉnh ảnh không có sẵn
                            echo "<div class='error'>Hỉnh ảnh không có sẵn.</div>";
                        } else {
                            //Hình ảnh có sẵn
                        ?>
                        <img src="<?php echo SITEURL; ?>/assets/img/food/<?php echo $image_name; ?>">
                        <?php
                        }
                        ?>
                </div>
                <div class="food-info">
                    <h3 class="food-title"><?php echo $title; ?></h3>
                    <span class="food-price"><?php echo number_format($price,3 ,'.','.'); ?>đ</span>
                    <p class="food-desc"><?php echo $description; ?></p>
                    <a href="<?php echo SITEURL; ?>order.php?food_id=<?php echo $id; ?>" class="btn-order">Đặt hàng ngay</a>
                </div>
            </div>
            <?php
            }
        } else {
            //Thức ăn không có sẵn 
            echo "<div class='error'>Món ăn không có sẵn.</div>";
        }

        ?>
        </div>
        <a href="<?php echo SITEURL; ?>foods.php" class="btn-all">Xem tất cả</a>
    </div>
</div>

<?php
include('./partials-front/footer.php');
?>

<div class="loader-container">
    <img src="./assets/img/loader.gif" alt=" ">
</div>
<a href="#" class="back-to-top">
    <i class="fas fa-angle-up"></i>
</a>
<script src="./assets/js/loader.js"></script>
<script src="./assets/js/slider.js"></script>
<script src="./assets/js/main.js"></script>
<script src="./assets/js/notyfication.js"></script>

</body>

</html>